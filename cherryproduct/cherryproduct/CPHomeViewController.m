//
//  ViewController.m
//  cherryproduct
//
//  Created by umessage on 14-8-4.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "CPHomeViewController.h"
#import "HttpNewBaseService.h"
#import "ClassListViewController.h"
#import "GroupDetailsViewController.h"
#import "ProductReviewViewController.h"
@interface CPHomeViewController ()

@end

@implementation CPHomeViewController


-(void)navtitle //顶端三个按钮
{
    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.myView.frame.size.width, 47)];
    view.backgroundColor = [UIColor whiteColor];
    UIImage* imagetitle = [UIImage imageNamed:@"navmainviewtitle"];
    UIImageView* imageviewtitle = [[UIImageView alloc] initWithFrame:CGRectMake((view.frame.size.width-imagetitle.size.width)/2, (view.frame.size.height-imagetitle.size.height)/2, imagetitle.size.width, imagetitle.size.height)];
    imageviewtitle.image = imagetitle;
    [view addSubview:imageviewtitle];
    
    UIImage* leftimage = [UIImage imageNamed:@"todaytitle"];
    UIImage* middleimage = [UIImage imageNamed:@"classtitle"];
    UIImage* rightimage = [UIImage imageNamed:@"fufruetitle"];
    int allwith = leftimage.size.width+middleimage.size.width+rightimage.size.width;
    int x = (self.myView.frame.size.width-allwith)/2;
    for(int i = 0 ; i < 3 ; i ++)
    {
        int with = 0;
        int height = 0;
        if(i==0)
        {
            with = leftimage.size.width;
            height = leftimage.size.height;
        }
        if(i==1)
        {
            with = middleimage.size.width;
            height = middleimage.size.height;
        }
        if(i==2)
        {
            with = rightimage.size.width;
            height = rightimage.size.height;
        }
        UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(x, (view.frame.size.height-leftimage.size.height)/2,with ,height)];
        button.backgroundColor = [UIColor clearColor];
        [button addTarget:self action:@selector(titletouchdown:) forControlEvents:UIControlEventTouchDown];
        button.tag = 200+i;
        if(i==0)
        {
            [button setBackgroundImage:leftimage forState:UIControlStateNormal];
            [button setTitle:@"今日" forState:UIControlStateNormal];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
        }
        if(i==1)
        {
            [button setBackgroundImage:nil forState:UIControlStateNormal];
            [button setTitle:@"口碑" forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
        }
        if(i==2)
        {
            [button setBackgroundImage:nil forState:UIControlStateNormal];
            [button setTitle:@"预告" forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
        }
        [view addSubview:button];
        x= x+with;
    }
    view.tag = 9821;
    [self.myView addSubview:view]; //顶部三个按钮导航
    
}

-(void)titletouchdown:(UIButton*)button //顶部三个按钮点击
{
    UIImage* leftimage = [UIImage imageNamed:@"todaytitle"];
    UIImage* middleimage = [UIImage imageNamed:@"classtitle"];
    UIImage* rightimage = [UIImage imageNamed:@"fufruetitle"];
    
    for(int i = 0 ; i < 3 ; i++)
    {
        UIButton* button = (UIButton*)[[self.myView viewWithTag:9821] viewWithTag:200+i];
        [button setBackgroundImage:nil forState:UIControlStateNormal];
        if(i==0)
        {
            [button setTitle:@"今日" forState:UIControlStateNormal];
            
        }
        if(i==1)
        {
            [button setTitle:@"口碑" forState:UIControlStateNormal];
            
        }
        if(i==2)
        {
            [button setTitle:@"预告" forState:UIControlStateNormal];
            
        }
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    

    
    [[self.view viewWithTag:1001] removeFromSuperview];//移除今日团购
    [[self.view viewWithTag:1002] removeFromSuperview];//移除今日口碑
    [[self.view viewWithTag:1003] removeFromSuperview];//移除未来团购

    if(button.tag==200)
    {
        [button setBackgroundImage:leftimage forState:UIControlStateNormal];
        [button setTitle:@"今日" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
       
        
        [[HttpNewBaseService sharedClient] getRequestOperation:@"" paramDictionary:nil onCompletion:^(NSString *responseString) {
            @try {
                NSDictionary *jsondic = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
                if([jsondic objectForKey:@"data"]&&([[jsondic objectForKey:@"data"] isKindOfClass:[NSDictionary class]]||[[jsondic objectForKey:@"data"] isKindOfClass:[NSArray class]]))
                {
                    datajson = [[NSMutableDictionary alloc] initWithDictionary:jsondic copyItems:YES];
                    MainTodayGroup* todaygroup = [[MainTodayGroup alloc] initWithFrame:CGRectMake(0, 47, self.myView.frame.size.width, self.myView.frame.size.height-47)];
                    todaygroup.tag = 1001;
                    todaygroup.delegate = self;
                    NSMutableDictionary* dicdata = [[NSMutableDictionary alloc] init];
                    [dicdata setObject:[jsondic objectForKey:@"data"] forKey:@"data"];
                    [todaygroup getusedata:dicdata];
                    [self.myView addSubview:todaygroup]; //添加今日团购
                    
                }
                else
                {
                    
                    [self showAlertView:@"温馨提示" subtitle:@"网络通讯失败！"];
                    
                }
                
                NSLog(@"%@",responseString);
            }
            @catch (NSException *exception) {
                
            }
            @finally {
                
            }
        } onError:^(NSError *error) {
            
        } onAnimated:NO];
        
//        [[HttpNewBaseService sharedClient] getRequestOperation:@"" paramDictionary:nil onCompletion:^(NSString *responseString) {
//            @try {
//                NSDictionary *jsondic = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
//                if([jsondic objectForKey:@"data"]&&([[jsondic objectForKey:@"data"] isKindOfClass:[NSDictionary class]]||[[jsondic objectForKey:@"data"] isKindOfClass:[NSArray class]]))
//                {
//                    datajson = [[NSMutableDictionary alloc] initWithDictionary:jsondic copyItems:YES];
//                    MainTodayGroup* todaygroup = [[MainTodayGroup alloc] initWithFrame:CGRectMake(0, 47, self.myView.frame.size.width, self.myView.frame.size.height-47)];
//                    todaygroup.tag = 1001;
//                    todaygroup.delegate = self;
//                    NSMutableDictionary* dicdata = [[NSMutableDictionary alloc] init];
//                    [dicdata setObject:[jsondic objectForKey:@"data"] forKey:@"data"];
//                    [todaygroup getusedata:dicdata];
//                    [self.myView addSubview:todaygroup]; //添加今日团购
//                    
//                }
//                else
//                {
//                   
//                    [self showAlertView:@"温馨提示" subtitle:@"网络通讯失败！"];
//                    
//                }
//                
//                NSLog(@"%@",responseString);
//            }
//            @catch (NSException *exception) {
//                [self showAlertView:@"温馨提示" subtitle:@"网络通讯失败！"];
//                
//            }
//            @finally {
//                ;
//            }
//            
//        } onError:^(NSError *error) {
//            ;
//        }];
        
    }
    if(button.tag==201)
    {
        [button setBackgroundImage:middleimage forState:UIControlStateNormal];
        [button setTitle:@"口碑" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        
        
        [[HttpNewBaseService sharedClient] getRequestOperation:@"index/category" paramDictionary:nil onCompletion:^(NSString *responseString) {
            @try {
                NSDictionary *jsondic = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
                if([jsondic objectForKey:@"data"]&&([[jsondic objectForKey:@"data"] isKindOfClass:[NSArray class]]))
                {
                    datajson = [[NSMutableDictionary alloc] initWithDictionary:jsondic copyItems:YES];
                    MainClassGroup*class = [[MainClassGroup alloc] initWithFrame:CGRectMake(0, 47, self.myView.frame.size.width, self.myView.frame.size.height-47)];
                    class.tag = 1002;
                    class.delegate = self;
                    [class getusedata:jsondic];
                    [self.myView addSubview:class]; //添加今日koubei
                    
                }
                else
                {
                    
                    [self showAlertView:@"温馨提示" subtitle:@"网络通讯失败！"];
                    
                }
                
                NSLog(@"%@",responseString);
            }
            @catch (NSException *exception) {
                [self showAlertView:@"温馨提示" subtitle:@"网络通讯失败！"];
                
            }
            @finally {
                ;
            }
            
        } onError:^(NSError *error) {
            ;
        } onAnimated:YES]; //今日团购
            
    }
    if(button.tag==202)
    {
        NSLog(@"今日预告");
        [button setBackgroundImage:rightimage forState:UIControlStateNormal];
        [button setTitle:@"预告" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [[HttpNewBaseService sharedClient] getRequestOperation:@"index/announce" paramDictionary:nil onCompletion:^(NSString *responseString) {
            @try {
                NSDictionary *jsondic = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
                if([jsondic objectForKey:@"data"]&&([[jsondic objectForKey:@"data"] isKindOfClass:[NSArray class]]))
                {
                    datajson = [[NSMutableDictionary alloc] initWithDictionary:jsondic copyItems:YES];
                    MainViewFutureGroup* mainfuture = [[MainViewFutureGroup alloc] initWithFrame:CGRectMake(0, 47, self.myView.frame.size.width, self.myView.frame.size.height-47)];
                    mainfuture.tag = 1003;
                    mainfuture.delegate = self;
                    [mainfuture getusedata:datajson];
                    [self.myView addSubview:mainfuture]; //添加未来团购
                    
                }
                else
                {
                    
                    [self showAlertView:@"温馨提示" subtitle:@"网络通讯失败！"];
                    
                }
                
                NSLog(@"%@",responseString);
            }
            @catch (NSException *exception) {
                [self showAlertView:@"温馨提示" subtitle:@"网络通讯失败！"];
                
            }
            @finally {
                ;
            }
        } onError:^(NSError *error) {
            ;
        } onAnimated:YES]; //今日团购
        

    
    }
         
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    isfuturnpage = NO;
   	// Do any additional setup after loading the view, typically from a nib.
    [self addNavBarButton:RIGHT_SEARCH_CENTRE];
    [self hidesBackButton:YES];
    [self navtitle]; //建立顶三个按钮
    UIButton* button = (UIButton*)[[self.myView viewWithTag:9821] viewWithTag:200];
    [self titletouchdown:button];//模拟点击今日团购
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)centreButtonAction:(UIButton *)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (((NSString*)[defaults objectForKey:@"USERNAME"]).length&&((NSString*)[defaults objectForKey:@"USERPASSWORD"])) {
        [self pushToViewController:@"MemberCentreViewController" animated:YES];
    }
    else
    {
        [self pushToViewController:@"LoginViewController" animated:YES];
    }

}
- (void)searchButtonAction:(UIButton *)sender
{
    
    [self pushToViewController:@"SearchViewController" animated:NO];
}

-(void)MainTodayGroupDownIndex:(int)indexdown //点击今日团购的序列
{
    isfuturnpage = NO;
    GroupDetailsViewController* detail = [[GroupDetailsViewController alloc] init];
    detail.stringtitle = [[[datajson objectForKey:@"data"] objectAtIndex:indexdown] objectForKey:@"title"];
    detail.timeover  =[[[[datajson objectForKey:@"data"] objectAtIndex:indexdown] objectForKey:@"end_time"] doubleValue];
    detail.stringid = [[[datajson objectForKey:@"data"] objectAtIndex:indexdown] objectForKey:@"id"];
    detail.istodaygroup = YES;
    detail.stringtitlecolor = [[[datajson objectForKey:@"data"] objectAtIndex:indexdown] objectForKey:@"color"];
    [self.navigationController pushViewController:detail animated:YES];
//    ProductReviewViewController* a = [[ProductReviewViewController alloc] init];
//    [self.navigationController pushViewController:a animated:YES];
}

-(void)MainClassGroupDownIndex:(int)indexdown numrow:(int)row //点击口碑的顺序
{
    isfuturnpage = NO;
    if(row==-1) //点击一级分类
    {
        NSDictionary*dic = [[datajson objectForKey:@"data"] objectAtIndex:indexdown];
        NSString* stringfirst =[dic objectForKey:@"id"];
        NSString* stringtitle = [dic objectForKey:@"title"];
        NSMutableDictionary* dicdataparam = [[NSMutableDictionary alloc] init];
        [dicdataparam setObject:stringfirst forKey:@"cid"];
        [dicdataparam setObject:stringtitle forKey:@"title"];
        
        ClassListViewController* list = [[ClassListViewController alloc] init];
        list.issearch = NO;
        [list getuseparamclasslistdata:dicdataparam];
        [self.navigationController pushViewController:list animated:YES];
    }
    else
    {
        
        NSDictionary*dic = [[datajson objectForKey:@"data"] objectAtIndex:indexdown];
        NSString* stringsec = [[[dic objectForKey:@"s_class"] objectAtIndex:row] objectForKey:@"id"];
        NSString* stringtitle = [[[dic objectForKey:@"s_class"] objectAtIndex:row] objectForKey:@"title"];

        NSMutableDictionary* dicdataparam = [[NSMutableDictionary alloc] init];
        [dicdataparam setObject:stringtitle forKey:@"title"];
        [dicdataparam setObject:stringsec forKey:@"cid"];

        ClassListViewController* list = [[ClassListViewController alloc] init];
        list.issearch = NO;
        [list getuseparamclasslistdata:dicdataparam];
        [self.navigationController pushViewController:list animated:YES];
        
    }
    NSLog(@"%d  %d",indexdown,row);

}

-(void)MainViewFutureGroupDownIndex:(int)indexdown numrow:(int)row //点击未来团购的某一项目 indexdown0为进入下一集页面1为上线提醒
{
    isfuturnpage = YES;

    if(indexdown==0) //进入团购详情
    {
        GroupDetailsViewController* detail = [[GroupDetailsViewController alloc] init];
        detail.stringtitle = [[[datajson objectForKey:@"data"] objectAtIndex:row] objectForKey:@"title"];
        detail.timeover  =[[[[datajson objectForKey:@"data"] objectAtIndex:row] objectForKey:@"start_time"] doubleValue];
        detail.stringid = [[[datajson objectForKey:@"data"] objectAtIndex:row] objectForKey:@"id"];
        detail.istodaygroup = NO;
        int temp =[[[[datajson objectForKey:@"data"] objectAtIndex:row] objectForKey:@"is_apply"] intValue];
        detail.isringup = temp;
        [self.navigationController pushViewController:detail animated:YES];
    }
    else
    {
        if([[[[datajson objectForKey:@"data"] objectAtIndex:row] objectForKey:@"is_apply"] isEqualToString:@"0"]) //为提醒
        {
            NSMutableDictionary* dic = [[NSMutableDictionary alloc] init];
            [dic setObject:[[[datajson objectForKey:@"data"] objectAtIndex:row] objectForKey:@"id"] forKey:@"id"];
            [[HttpNewBaseService sharedClient] getRequestOperation:@"groupon/alarm_join" paramDictionary:dic onCompletion:^(NSString *responseString) {
                NSDictionary *jsondic = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
                if([[jsondic objectForKey:@"status"] intValue]==1)//成功
                {
                    [self showAlertView:@"温馨提示" subtitle:[jsondic objectForKey:@"message"]];
                    [self viewWillAppear:YES];

                }
                else
                {
                     [self showAlertView:@"温馨提示" subtitle:[jsondic objectForKey:@"message"]];
                }
                
            } onError:^(NSError *error) {
                ;
            } onAnimated:YES];
        }
        else //以提醒
        {
            NSMutableDictionary* dic = [[NSMutableDictionary alloc] init];
            [dic setObject:[[[datajson objectForKey:@"data"] objectAtIndex:row] objectForKey:@"id"] forKey:@"id"];
            [[HttpNewBaseService sharedClient] getRequestOperation:@"groupon/alarm_cancel" paramDictionary:dic onCompletion:^(NSString *responseString) {
                NSDictionary *jsondic = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
                if([[jsondic objectForKey:@"status"] intValue]==1)//成功
                {
                    [self showAlertView:@"温馨提示" subtitle:[jsondic objectForKey:@"message"]];
                    [self viewWillAppear:YES];
                    
                }
                else
                {
                    [self showAlertView:@"温馨提示" subtitle:[jsondic objectForKey:@"message"]];
                }
                
            } onError:^(NSError *error) {
                ;
            } onAnimated:YES];
        }
        
    }
  

    NSLog(@"%d   %d",indexdown,row);
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(isfuturnpage)
    {
        
        UIButton* button = (UIButton*)[[self.myView viewWithTag:9821] viewWithTag:202];
        [self titletouchdown:button];//预告

    }
}

@end
