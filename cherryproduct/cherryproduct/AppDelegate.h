//
//  AppDelegate.h
//  cherryproduct
//
//  Created by umessage on 14-8-4.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CPHomeViewController.h"
#import "WeiboSDK.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController * navigationCongtroller;
@property (strong, nonatomic) CPHomeViewController* homeViewController;
@property (strong,nonatomic) NSString* wbtoken;
@end
