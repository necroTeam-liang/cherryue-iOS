//
//  UIPageIndexcustmView.m
//  cherryproduct
//
//  Created by umessage on 14-8-13.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "UIPageIndexcustmView.h"

@implementation UIPageIndexcustmView
@synthesize maxnum;
@synthesize upimagestringname;
@synthesize nextimagestringname;
@synthesize spacingdistance;

-(void)startdiandian //开始绘制点点
{
    UIImage* imageup = [UIImage imageNamed:upimagestringname];
    UIImage* imagenext = [UIImage imageNamed:nextimagestringname];
    int x = (self.frame.size.width-imageup.size.width*maxnum-((maxnum-1)*spacingdistance))/2;
    for(int i = 0 ; i < maxnum ; i++)
    {
        UIImageView*  imageview = [[UIImageView alloc] initWithFrame:CGRectMake(x, (self.frame.size.height-imageup.size.height)/2 , imageup.size.width, imageup.size.height)];
        
        if(i==0)
        {
            upnum = i;
            imageview.image = imageup;
        }
        else
        {
            imageview.image = imagenext;
        }
        imageview.tag = 100+i;
        [self addSubview:imageview];
        x = x+imageview.frame.size.width+spacingdistance;
    }
}
-(void)setupIndexpage:(int)num //选中的点点
{
    UIImage* imageup = [UIImage imageNamed:upimagestringname];
    UIImage* imagenext = [UIImage imageNamed:nextimagestringname];
    UIImageView* imageviewnext = (UIImageView*)[self viewWithTag:100+upnum];//去除上次的标记图案
    imageviewnext.image = imagenext;
    UIImageView* imageview = (UIImageView*)[self viewWithTag:100+num];
    imageview.image = imageup;

    upnum = num;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
