//
//  UIPageIndexcustmView.h
//  cherryproduct
//
//  Created by umessage on 14-8-13.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIPageIndexcustmView : UIView  //图片圆圈索引view
{
    int upnum; //选中的点点标记
}
@property(copy,nonatomic) NSString* upimagestringname; //选中的点图片名字
@property(copy,nonatomic) NSString* nextimagestringname; //未选中的图片名字
@property(assign,nonatomic) int maxnum; //显示多少个点点
@property(assign,nonatomic) int spacingdistance; //点点的间隔距离
-(void)setupIndexpage:(int)num; //选中的第几个
-(void)startdiandian; //开始绘制点点
@end
