//
//  GroupDetailsViewController.m
//  cherryproduct
//
//  Created by umessage on 14-8-13.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "GroupDetailsViewController.h"
#import "UIImageView+AFNetworking.h"
#import "HttpNewBaseService.h"
#import "UIButton+AFNetworking.h"
#import "BuyProductViewController.h"
@interface GroupDetailsViewController ()

@end

@implementation GroupDetailsViewController
@synthesize stringtitle;
@synthesize timeover;
@synthesize istodaygroup;
@synthesize stringid;
@synthesize stringtitlecolor;
@synthesize isringup;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)buttondown:(UIButton*)button //点击轮播图
{
    NSLog(@"%d",button.tag-100);
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setTitle:self.stringtitle];
    
}


-(void)timeoverdown:(NSTimer*) timertemp //倒计时
{
    double timetemp = self.timeover - [[NSDate date] timeIntervalSince1970];
    UILabel*label = (UILabel*)[self.navigationBar viewWithTag:6512];
    NSDateFormatter* fortimemat = [[NSDateFormatter alloc] init];
    [fortimemat setDateFormat:@"HH:mm:ss"];
    if(timetemp>0)
    {
        
        label.text= [fortimemat stringFromDate:[NSDate dateWithTimeIntervalSince1970:timetemp]];
        
    }
    else
    {
        label.text = @"已过期";
        [timer invalidate];
        
    }

    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}


-(void)shareButtonAction:(UIButton*)button //分享
{
    [self umengShare:@"樱桃" icon:nil];
}

-(void)ringButtonAction:(UIButton*)button//已经设立提醒
{
 
    
    NSMutableDictionary* dic = [[NSMutableDictionary alloc] init];
    [dic setObject:self.stringid forKey:@"id"];
    [[HttpNewBaseService sharedClient] getRequestOperation:@"groupon/alarm_cancel" paramDictionary:dic onCompletion:^(NSString *responseString) {
        NSDictionary *jsondic = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
        if([[jsondic objectForKey:@"status"] intValue]==1)//成功
        {
            [self showAlertView:@"温馨提示" subtitle:[jsondic objectForKey:@"message"]];
            [button removeFromSuperview];
            
            [self addNavBarButton:RIGHT_DOWNRING];
        }
        else
        {
            [self showAlertView:@"温馨提示" subtitle:[jsondic objectForKey:@"message"]];
        }
        
    } onError:^(NSError *error) {
        ;
    } onAnimated:YES];


    
   
    
}

-(void)ringdownButtonAction:(UIButton*)button //没有设置提醒
{
    
   

    NSMutableDictionary* dic = [[NSMutableDictionary alloc] init];
    [dic setObject:self.stringid forKey:@"id"];
    [[HttpNewBaseService sharedClient] getRequestOperation:@"groupon/alarm_join" paramDictionary:dic onCompletion:^(NSString *responseString) {
        NSDictionary *jsondic = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
        if([[jsondic objectForKey:@"status"] intValue]==1)//成功
        {
           
            [self showAlertView:@"温馨提示" subtitle:[jsondic objectForKey:@"message"]];
            [button removeFromSuperview];
            [self addNavBarButton:RIGHT_RING];
            
        }
        else
        {
            [self showAlertView:@"温馨提示" subtitle:[jsondic objectForKey:@"message"]];
        }
        
    } onError:^(NSError *error) {
        ;
    } onAnimated:YES];
    

    

}


- (void)viewDidLoad
{
    [super viewDidLoad];
    if(self.istodaygroup) //今日团购过来的页面
    {
        [self addNavBarButton:RIGHT_SHARE];
        
    }
    else
    {
        if(self.isringup)
        {
            [self addNavBarButton:RIGHT_RING];

        }
        else
        {
            [self addNavBarButton:RIGHT_DOWNRING];
        }

    }
    
    if(self.istodaygroup)
    {
        UIImageView* imageview = [self navigationBar];
        imageview.backgroundColor = [self colorWithHexString:self.stringtitlecolor];
        imageview.image = nil;
    }
   
    
    double timetemp = self.timeover - [[NSDate date] timeIntervalSince1970];
    UILabel* labeltime = [[UILabel alloc] initWithFrame:CGRectMake(0, self.navigationBar.frame.size.height-15, self.myView.frame.size.width, 15)];
    labeltime.font = [UIFont systemFontOfSize:12];
    labeltime.textAlignment = NSTextAlignmentCenter;
    labeltime.backgroundColor = [UIColor clearColor];
    labeltime.textColor = [UIColor blackColor];
    labeltime.tag = 6512;
    NSDateFormatter* fortimemat = [[NSDateFormatter alloc] init];
    [fortimemat setDateFormat:@"HH:mm:ss"];
    if(timetemp>0)
    {
        
        labeltime.text= [fortimemat stringFromDate:[NSDate dateWithTimeIntervalSince1970:timetemp]];
        timer =  [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timeoverdown:) userInfo:nil repeats:YES];

    }
    else
    {
        if(self.istodaygroup)
        {
            labeltime.text = @"已过期";

        }
        else
        {
            labeltime.text = @"已开始";
        }

    }
    
    [self.navigationBar addSubview:labeltime];
    
    NSMutableDictionary* dicdataparam = [[NSMutableDictionary alloc] init];
    [dicdataparam setValue:self.stringid forKeyPath:@"id"];
    [[HttpNewBaseService sharedClient] getRequestOperation:@"Groupon/groupon_detail" paramDictionary:dicdataparam onCompletion:^(NSString *responseString) {
        @try {
            
            NSDictionary *jsondic = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
            dicdata = [[NSMutableDictionary alloc] initWithDictionary:jsondic copyItems:YES];
            if([dicdata objectForKey:@"data"]&&[[dicdata objectForKey:@"data"] isKindOfClass:[NSDictionary class]])
            {
                UIScrollView* scrollview; //轮播图
                UIView* view; //说明详情
                if([[dicdata objectForKey:@"data"] objectForKey:@"groupon"]&&[[[dicdata objectForKey:@"data"] objectForKey:@"groupon"] isKindOfClass:[NSDictionary class]])
                {
                    
                    NSDictionary* dicimage = [[dicdata objectForKey:@"data"] objectForKey:@"groupon"];
                    if([dicimage objectForKey:@"groupon_photo"]&&[[dicimage objectForKey:@"groupon_photo"] isKindOfClass:[NSArray class]])
                    {
                        NSArray* arrayimage = [dicimage objectForKey:@"groupon_photo"];//轮播图
                        scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.myView.frame.size.width, 115)];
                        scrollview.showsHorizontalScrollIndicator = NO;
                        scrollview.showsVerticalScrollIndicator = NO;
                        scrollview.pagingEnabled = YES;
                        scrollview.backgroundColor = [UIColor whiteColor];
                        scrollview.contentSize = CGSizeMake(self.myView.frame.size.width*[arrayimage count], 115);
                        scrollview.delegate = self;
                        int x = 0;
                        for(int i = 0 ; i < [arrayimage count] ;i++)
                        {
                            UIButton* button = [[UIButton alloc] initWithFrame:CGRectMake(x, 0, scrollview.frame.size.width, scrollview.frame.size.height)];
                            NSString* stringimage = [NSString stringWithFormat:@"%@%@",JPG,[[arrayimage objectAtIndex:i] objectForKey:@"img"]];
                            [button setBackgroundImageForState:UIControlStateNormal withURL:[NSURL URLWithString:stringimage] placeholderImage:[UIImage imageNamed:@"imagedegrouploading"]];
                            [button addTarget:self action:@selector(buttondown:) forControlEvents:UIControlEventTouchUpInside];
                            button.tag = 100+i;
                            [scrollview addSubview:button];
                            x = x+scrollview.frame.size.width;
                        }
                        [self.myView addSubview:scrollview]; //添加导航图
                        
                        pageindex = [[UIPageIndexcustmView alloc] initWithFrame:CGRectMake(0, scrollview.frame.size.height-12,scrollview.frame.size.width, 12)];
                        pageindex.upimagestringname = @"pagegroupdetail";
                        pageindex.nextimagestringname = @"pagenextgroupdetail";
                        pageindex.spacingdistance = 5;
                        pageindex.maxnum = [arrayimage count];
                        [pageindex startdiandian];
                        [self.myView addSubview:pageindex];//添加点点
                        // Do any additional setup after loading the view.
                    }
                    
                    
                    view = [[UIView alloc] initWithFrame:CGRectMake(0, scrollview.frame.size.height, self.myView.frame.size.width, 75)];
                    view.backgroundColor = [UIColor whiteColor];
                    UIImageView*imageview = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cherrygrounpdetail"]];
                    imageview.frame = CGRectMake((view.frame.size.width-imageview.image.size.width)/2, 5, imageview.image.size.width,imageview.image.size.height);
                    [view addSubview:imageview];
                    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, imageview.frame.size.height+imageview.frame.origin.y, view.frame.size.width, view.frame.size.height-(imageview.frame.size.height+imageview.frame.origin.y))];
                    label.text =  [dicimage objectForKey:@"des"];
                    label.backgroundColor = [UIColor clearColor];
                    label.font = [UIFont systemFontOfSize:14];
                    label.textAlignment = NSTextAlignmentCenter;
                    label.numberOfLines = 0;
                    label.textColor = [UIColor blackColor];
                    [view addSubview:label];
                    [self.myView addSubview:view];

                  
                }
                
                if([[dicdata objectForKey:@"data"] objectForKey:@"goods"]&&[[[dicdata objectForKey:@"data"] objectForKey:@"goods"] isKindOfClass:[NSArray class]])
                {
                    
                    UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
                    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
                    //注册
                    UICollectionView* collectionview = [[UICollectionView alloc] initWithFrame:CGRectMake(0, view.frame.origin.y+view.frame.size.height, self.myView.frame.size.width,self.myView.frame.size.height-(view.frame.origin.y+view.frame.size.height)) collectionViewLayout:flowLayout];
                    [collectionview registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
                    collectionview.delegate = self;
                    collectionview.dataSource  = self;
                    collectionview.backgroundColor = [UIColor whiteColor];
                    [self.myView addSubview:collectionview]; //分类列表
                }
            }
        }
        @catch (NSException *exception) {
            [URBAlertView dialogWithTitle:@"温馨提示" subtitle:@"网络通讯失败！"];
            
        }
        @finally {
            ;
        }
    } onError:^(NSError *error) {
        ;
    } onAnimated:YES]; //团购商品详情列表，首页点击进入
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int x = scrollView.contentOffset.x;
    [pageindex setupIndexpage:x/self.myView.frame.size.width];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark -- UICollectionViewDataSource
//定义展示的UICollectionViewCell的个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[[dicdata objectForKey:@"data"] objectForKey:@"goods"] count];
}
//定义展示的Section的个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
//每个UICollectionView展示的内容
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* string = @"cell";
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:string forIndexPath:indexPath];
    NSArray* array = [[dicdata objectForKey:@"data"] objectForKey:@"goods"];
    NSDictionary* dic = [array objectAtIndex:indexPath.row];
    UIImageView* imageview;
    NSString* stringimageurl = [NSString stringWithFormat:@"%@%@",JPG,[[dic objectForKey:@"goods"] objectForKey:@"photo"]];
    if([cell viewWithTag:101])
    {
        imageview = (UIImageView*)[cell viewWithTag:101];
        [imageview setImageWithURL:[NSURL URLWithString:stringimageurl] placeholderImage:[UIImage imageNamed:@"deimagegroupdeloading"]];
    }
    else
    {
        imageview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, 90)];
        [imageview setImageWithURL:[NSURL URLWithString:stringimageurl] placeholderImage:[UIImage imageNamed:@"deimagegroupdeloading"]];
        imageview.backgroundColor = [UIColor clearColor];
        imageview.tag = 101;
        [cell addSubview:imageview];//添加商品图片
    }
    
    UILabel* labeltitle;
    if([cell viewWithTag:102])
    {
        labeltitle = (UILabel*)[cell viewWithTag:102];
        labeltitle.text = [[dic objectForKey:@"goods"] objectForKey:@"name"];
        CGSize size = [labeltitle.text sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(cell.frame.size.width, 999) lineBreakMode:UILineBreakModeWordWrap];
        if(size.height>35)
        {
            size.height = 35;
        }
        labeltitle.frame = CGRectMake(0, imageview.frame.origin.y+imageview.frame.size.height, size.width, size.height);

    }
    else
    {
        labeltitle = [[UILabel alloc] initWithFrame:CGRectMake(0, imageview.frame.origin.y+imageview.frame.size.height, cell.frame.size.width, 35)];
        labeltitle.tag = 102;
        labeltitle.text = [[dic objectForKey:@"goods"] objectForKey:@"name"];
        CGSize size = [labeltitle.text sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(cell.frame.size.width, 999) lineBreakMode:UILineBreakModeWordWrap];
        if(size.height>35)
        {
            size.height = 35;
        }
        labeltitle.frame = CGRectMake(0, imageview.frame.origin.y+imageview.frame.size.height, size.width, size.height);
        labeltitle.textColor = [UIColor blackColor];
        labeltitle.textAlignment = NSTextAlignmentLeft;
        labeltitle.font = [UIFont systemFontOfSize:14];
        labeltitle.numberOfLines = 0;
        labeltitle.backgroundColor = [UIColor clearColor];
        [cell addSubview:labeltitle]; //添加商品title
    }
    
    UILabel* labelmon;
    if([cell viewWithTag:103])
    {
        labelmon = (UILabel*)[cell viewWithTag:103];
        labelmon.text = [NSString stringWithFormat:@"¥%@",[dic objectForKey:@"price"]];
        CGSize size = [labelmon.text sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(999, 999) lineBreakMode:UILineBreakModeWordWrap];
        labelmon.frame = CGRectMake(0, cell.frame.size.height-size.height, size.width, size.height);
    }
    else
    {
        labelmon = [[UILabel alloc] initWithFrame:CGRectMake(0, labeltitle.frame.origin.y+labeltitle.frame.size.height, 100, 16)];
        labelmon.tag = 103;
        labelmon.text = [NSString stringWithFormat:@"¥%@",[dic objectForKey:@"price"]];
        CGSize size = [labelmon.text sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(999, 999) lineBreakMode:UILineBreakModeWordWrap];
        labelmon.frame = CGRectMake(0, cell.frame.size.height-size.height, size.width, size.height);
        labelmon.textColor = [UIColor redColor];
        labelmon.textAlignment = NSTextAlignmentLeft;
        labelmon.font = [UIFont systemFontOfSize:15];
        labelmon.backgroundColor = [UIColor clearColor];
        [cell addSubview:labelmon]; //添加商品价格
    }
    
    
    UILabel* labelmonyuan;
    if([cell viewWithTag:104])
    {
        labelmonyuan = (UILabel*)[cell viewWithTag:104];
        labelmonyuan.text = [NSString stringWithFormat:@"¥%@",[[dic objectForKey:@"goods"] objectForKey:@"price"]];
        CGSize size = [labelmonyuan.text sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(999, 999) lineBreakMode:UILineBreakModeWordWrap];
        labelmonyuan.frame = CGRectMake(labelmon.frame.origin.x+labelmon.frame.size.width+5, cell.frame.size.height-size.height-1, size.width, size.height);
    }
    else
    {
        labelmonyuan = [[UILabel alloc] initWithFrame:CGRectMake(0, labeltitle.frame.origin.y+labeltitle.frame.size.height+3, 100, 16)];
        labelmonyuan.tag = 104;
        labelmonyuan.text = [NSString stringWithFormat:@"¥%@",[[dic objectForKey:@"goods"] objectForKey:@"price"]];
        CGSize size = [labelmonyuan.text sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(999, 999) lineBreakMode:UILineBreakModeWordWrap];
        labelmonyuan.frame = CGRectMake(labelmon.frame.origin.x+labelmon.frame.size.width+5, cell.frame.size.height-size.height-1, size.width, size.height);
        labelmonyuan.textColor = [UIColor blackColor];
        labelmonyuan.textAlignment = NSTextAlignmentLeft;
        labelmonyuan.font = [UIFont systemFontOfSize:12];
        labelmonyuan.backgroundColor = [UIColor clearColor];
        [cell addSubview:labelmonyuan]; //添加商品原价格
    }
    
    
    UIImageView* imageviewhonggan;
    if([cell viewWithTag:105])
    {
        imageviewhonggan = (UIImageView*)[cell viewWithTag:105];
        imageviewhonggan.frame = CGRectMake(labelmonyuan.frame.origin.x-2, labelmonyuan.frame.origin.y+(labelmonyuan.frame.size.height/2), labelmonyuan.frame.size.width+4, 0.5);
    }
    else
    {
        imageviewhonggan = [[UIImageView alloc] initWithFrame:CGRectMake(labelmonyuan.frame.origin.x-2, labelmonyuan.frame.origin.y+(labelmonyuan.frame.size.height/2), labelmonyuan.frame.size.width+4, 1)];
        imageviewhonggan.backgroundColor = [UIColor blackColor];
        imageviewhonggan.tag = 105;
        [cell addSubview:imageviewhonggan];//添加横杠
    }

    UIButton* button;
    if([cell viewWithTag:106])
    {
        button = (UIButton*)[cell viewWithTag:106];
        button.titleLabel.tag = indexPath.row+999;
        

    }
    else
    {
        UIImage* imagebutton = [UIImage imageNamed:@"donetouchgropdetail"];
        button = [[UIButton alloc] init];
        button.frame = CGRectMake(cell.frame.size.width-imagebutton.size.width, cell.frame.size.height-imagebutton.size.height, imagebutton.size.width, imagebutton.size.height);
        [button setTitle:@"立即参团" forState:UIControlStateNormal];
        button.titleLabel.numberOfLines = 0;
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:11];
        button.titleLabel.textAlignment = NSTextAlignmentCenter;
        [button setTitleEdgeInsets:UIEdgeInsetsMake(0,7,0,7)];
        [button setBackgroundImage:imagebutton forState:UIControlStateNormal];
        [button addTarget:self action:@selector(toucdownbutton:) forControlEvents:UIControlEventTouchDown];
        button.backgroundColor = [UIColor clearColor];
        button.tag = 106;
        button.titleLabel.tag = indexPath.row+999;
        [cell addSubview:button];//添加立即参团
        
        if(self.istodaygroup)
        {
            ;
        }
        else
        {
            UIImage* imagebuttonup = [UIImage imageNamed:@"donegroupdetail"];
            [button setTitle:@"暂未开团" forState:UIControlStateNormal];
            [button setBackgroundImage:imagebuttonup forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

 
        }
    }
    
    
    cell.backgroundColor = [UIColor whiteColor];
    
    return cell;
}

-(void)toucdownbutton:(UIButton*)button //立即参团
{
    int index = button.titleLabel.tag-999;
    
    BuyProductViewController* buy = [[BuyProductViewController alloc] init];
    buy.stringurl =[[[[[dicdata objectForKey:@"data"] objectForKey:@"goods"] objectAtIndex:index] objectForKey:@"goods"] objectForKey:@"buy_url"];
    buy.stringid = [[[[[dicdata objectForKey:@"data"] objectForKey:@"goods"] objectAtIndex:index] objectForKey:@"goods"] objectForKey:@"id"];
    NSLog(@"%@,%@",buy.stringid,buy.stringurl);
    [self.navigationController pushViewController:buy animated:YES];
    NSLog(@"%d",index);
}
#pragma mark --UICollectionViewDelegateFlowLayout
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(130, 145);
}
//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 20, 0, 20);
}
#pragma mark --UICollectionViewDelegate
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    BuyProductViewController* buy = [[BuyProductViewController alloc] init];
    buy.stringurl =[[[[[dicdata objectForKey:@"data"] objectForKey:@"goods"] objectAtIndex:indexPath.row] objectForKey:@"goods"] objectForKey:@"buy_url"];
    buy.stringid = [[[[[dicdata objectForKey:@"data"] objectForKey:@"goods"] objectAtIndex:indexPath.row] objectForKey:@"goods"] objectForKey:@"id"];
    NSLog(@"%@,%@",buy.stringid,buy.stringurl);
    [self.navigationController pushViewController:buy animated:YES];
    
}
//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


//设置字体颜色
- (UIColor *) colorWithHexString: (NSString *) stringToConvert
{
    NSString *cString = [[stringToConvert stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    
    if ([cString length] < 6)
        return [UIColor whiteColor];
    if ([cString hasPrefix:@"#"])
        cString = [cString substringFromIndex:1];
    if ([cString length] != 6)
        return [UIColor whiteColor];
    
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

-(void)dealloc
{
    if([timer isValid])
    {
        [timer invalidate];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
