//
//  GroupDetailsViewController.h
//  cherryproduct
//
//  Created by umessage on 14-8-13.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIPageIndexcustmView.h"
@interface GroupDetailsViewController : BaseViewController<UIScrollViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    NSMutableDictionary* dicdata; //页面所需数据
    UIPageIndexcustmView* pageindex; //点点
    NSTimer* timer;
}
@property(copy,nonatomic) NSString* stringid; //请求参数
@property(copy,nonatomic) NSString* stringtitle; //抬头名字
@property(assign,nonatomic) double timeover; //结束时间
@property(assign,nonatomic) BOOL istodaygroup; //是否今日团购
@property(copy,nonatomic) NSString* stringtitlecolor; //title颜色
@property(assign,nonatomic) BOOL isringup; //是否已经提醒
@end
