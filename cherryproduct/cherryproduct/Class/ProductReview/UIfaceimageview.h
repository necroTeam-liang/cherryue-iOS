//
//  UIfaceimageview.h
//  cherryproduct
//
//  Created by umessage on 14-8-27.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

@protocol UIfaceimageviewdelegate <NSObject>

-(void)UIfaceimageviewdelegate:(int)index;

@end
#import <UIKit/UIKit.h>
@interface UIfaceimageview : UIView<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    ;
}
@property(assign,nonatomic)id<UIfaceimageviewdelegate> delegate;
@end
