//
//  ProductReviewViewController.m
//  cherryproduct
//
//  Created by umessage on 14-8-18.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "ProductReviewViewController.h"
@interface ProductReviewViewController ()

@end

@implementation ProductReviewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}





-(void)initviewstyle
{
    int y=5;
    UIImage* upimage = [UIImage imageNamed:@"productreviewsup"];
    UIImage* downimage = [UIImage imageNamed:@"productreviewsdown"];
    UIImageView* imageview ; //分割线
    for(int i = 0 ; i < 4 ;i++)
    {
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, y, 95, 16)];
        label.textColor = [UIColor blackColor];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentRight;
        label.font = [UIFont systemFontOfSize:16];
        if(i==0)
        {
            label.text = @"好用指数";
        }
        else if(i==1)
        {
            label.text = @"使用度指数";

        }
        else if(i==2)
        {
            label.text = @"性价比指数";

        }
        else
        {
            label.text = @"安全性指数";

        }
        
        int x = label.frame.origin.x+label.frame.size.width+15;//butt哼左边
        NSMutableArray* array = [[NSMutableArray alloc] init];
        for(int j = 0 ; j < 5 ;j++)
        {
            UIImageView* imageviewstar = [[UIImageView alloc] initWithFrame:CGRectMake(x, label.frame.origin.y+(label.frame.size.height-upimage.size.height)/2, upimage.size.width, upimage.size.height)];
            imageviewstar.backgroundColor = [UIColor clearColor];
            imageviewstar.tag = j+(100+100*i); //100起步依次向下最多到500标记
            imageview.userInteractionEnabled = YES;
            if(j==4)
            {
                imageviewstar.image = downimage;
                
            }
            else
            {
                imageviewstar.image = upimage;

            }
            [self.myView addSubview:imageviewstar]; //添加评分按钮
            x = x + imageviewstar.frame.size.width+15;
            
            NSMutableDictionary* dic = [[NSMutableDictionary alloc] init];
            [dic setObject:[NSNumber numberWithInt:imageviewstar.frame.origin.x+imageviewstar.frame.size.width] forKey:@"starx"];
            [dic setObject:[NSNumber numberWithInt:imageviewstar.frame.origin.y+imageviewstar.frame.size.height] forKey:@"stary"];
            [dic setObject:[NSNumber numberWithInt:4] forKey:@"star"];
            [dic setObject:[NSNumber numberWithInt:imageviewstar.tag] forKey:@"startag"];
            [array addObject:dic];
        }
        [arraystar addObject:array];
        
        [self.myView addSubview:label]; //添加评级说明
        y = y+ label.frame.size.height+10;
        
        if(i == 3)
        {
            starty = label.frame.origin.y+label.frame.size.height;
            imageview = [[UIImageView alloc] initWithFrame:CGRectMake(10, label.frame.origin.y+label.frame.size.height+5, self.myView.frame.size.width-20, 0.5)];
            imageview.backgroundColor = [UIColor blackColor];
            [self.myView addSubview:imageview];//添加分割线
        }
    }
    
    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.myView.frame.size.width, self.myView.frame.size.height)];
    view.backgroundColor = [UIColor whiteColor];
    view.tag = 9123;
    view.hidden = YES;
    [self.myView addSubview:view];
    
    
    scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, imageview.frame.origin.y+imageview.frame.size.height+5, self.myView.frame.size.width, self.myView.frame.size.height-(imageview.frame.origin.y+imageview.frame.size.height+5))];
    scrollview.backgroundColor = [UIColor whiteColor];
    scrollview.contentSize = CGSizeMake(scrollview.frame.size.width,scrollview.frame.size.height);
    [self.myView addSubview:scrollview];
    
    
    textview = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, self.myView.frame.size.width, self.myView.frame.size.height-(imageview.frame.origin.y+imageview.frame.size.height+5))];
    textview.font = [UIFont systemFontOfSize:12];
    textview.textColor = [UIColor blackColor];
    textview.backgroundColor = [UIColor whiteColor];
    textview.returnKeyType = UIReturnKeyDone;//返回键的类型
    textview.keyboardType = UIKeyboardTypeDefault;//键盘类型
    textview.scrollEnabled = NO;//是否可以拖动
    textview.userInteractionEnabled = YES;
    textview.delegate = self;
    textview.text = @"请输入评论内容……";
    [scrollview addSubview:textview];//加入到整个页面中

}

-(void)textviewbuttonreview //建立照相机试图
{
    UIImage* imageback = [UIImage imageNamed:@"backviewreview"];
    UIImageView* imageview = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.myView.frame.size.height-textviewboardheight-imageback.size.height, self.myView.frame.size.width, imageback.size.height)];
    imageview.userInteractionEnabled = YES;
    imageview.image = imageback;
    imageview.tag = 7618;
    [self.myView addSubview:imageview];
    
    int x = 5;
    UIImage* imagecar = [UIImage imageNamed:@"carmerreview"];
    UIImage* imageface = [UIImage imageNamed:@"facereview"];
    UIImage* imagelib = [UIImage imageNamed:@"libreview"];
    for(int i = 0 ; i < 3 ; i ++)
    {
        UIButton* button = [[UIButton alloc] initWithFrame:CGRectMake(x, 0, 40, 40)];
        button.tag = 931+i;
        [button addTarget:self action:@selector(textviewboardreview:) forControlEvents:UIControlEventTouchUpInside];
        int temp= 0;
        if(i==0)
        {
            [button setBackgroundImage:imagecar forState:UIControlStateNormal];
            button.frame = CGRectMake(x,(imageview.frame.size.height-imagecar.size.height)/2, imagecar.size.width, imagecar.size.height);
            temp = x+imagecar.size.width+20;
        }
        else if(i==1)
        {
            [button setBackgroundImage:imagelib forState:UIControlStateNormal];
            button.frame = CGRectMake(x,(imageview.frame.size.height-imagelib.size.height)/2, imagelib.size.width, imagelib.size.height);
            temp = x+imagelib.size.width+20;

        }
        else
        {
            [button setBackgroundImage:imageface forState:UIControlStateNormal];
            button.frame = CGRectMake(x,(imageview.frame.size.height-imageface.size.height)/2, imageface.size.width, imageface.size.height);
            temp = x+imageface.size.width+20;

        }
        
        [imageview addSubview:button];
        x = temp;
    }
}

-(void)textviewboardreview:(UIButton*)button
{
    int temp = button.tag-931;
    if(temp==0) //相机
    {
        if(faceimageview)
        {
            [faceimageview removeFromSuperview];
            faceimageview = nil;
        }

        BOOL isCamera = [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront];
        if (isCamera) {
            [textview resignFirstResponder];
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.view.tag = 1011;//相机
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            imagePicker.delegate = self;
            // 编辑模式
            imagePicker.allowsEditing = YES;
            
            [self  presentViewController:imagePicker animated:YES completion:^{
            }];
        }
        else
        {
            [self showAlertView:@"温馨提示" subtitle:@"您的设备不支持此功能"];
        }

    }
    else if(temp==1) //图册
    {
        if(faceimageview)
        {
            [faceimageview removeFromSuperview];
            faceimageview = nil;
        }

        [textview resignFirstResponder];
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.view.tag = 1012;//相册
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.delegate = self;
        [self  presentViewController:imagePicker animated:YES completion:^{
        }];
        

    }
    else //表情
    {
        if(faceimageview)
        {
            [faceimageview removeFromSuperview];
            faceimageview = nil;
        }
        else
        {
            UIWindow* wind = [[UIApplication sharedApplication].windows objectAtIndex:[[UIApplication sharedApplication].windows count]-1];
            NSLog(@"%lf",wind.frame.size.height);
            faceimageview = [[UIfaceimageview alloc] initWithFrame:CGRectMake(0, wind.frame.size.height- textviewboardheight, self.myView.frame.size.width, textviewboardheight)];
            faceimageview.delegate = self;
            [wind addSubview:faceimageview];
        }
       
    }
}

-(void)UIfaceimageviewdelegate:(int)index
{
    [faceimageview removeFromSuperview];
    faceimageview = nil;
    if([textview.text isEqualToString:@"请输入评论内容……"])
    {
        textview.text = [NSString stringWithFormat:@"[qqface]%d[/qqface]",index];
    }
    else
    {
        NSMutableString* string = [[NSMutableString alloc] init];
        [string appendString:textview.text];
        [string appendFormat:@"[qqface]%d[/qqface]",index];
        textview.text = string;
    }
    
    [self textViewDidChange:textview];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    NSLog(@"%@", info);
    UIImage *image = nil;
    
    if(picker.view.tag == 1011) //相机
    {
        image = [info objectForKey:UIImagePickerControllerEditedImage];

    }
    else //图片
    {
        image = [info objectForKey:UIImagePickerControllerOriginalImage];

    }
    
    CGSize size = [textview.text sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(scrollview.frame.size.width-10, 999) lineBreakMode:NSLineBreakByWordWrapping];
    
    if(imageviewphoto)
    {
        imageviewphoto.image = image;
        imageviewphoto.frame = CGRectMake(5, size.height+20, 100, 100);
    }
    else
    {
        imageviewphoto = [[UIImageView alloc] init];
        imageviewphoto.frame = CGRectMake(5, size.height+20, 100, 100);
        imageviewphoto.image = image;
        imageviewphoto.backgroundColor = [UIColor redColor];
        [scrollview addSubview:imageviewphoto];
    }
    scrollview.contentSize = CGSizeMake(scrollview.frame.size.width, size.height+20+100);
    if(scrollview.frame.size.height<(size.height+20+100))
    {
        [scrollview setContentOffset:CGPointMake(scrollview.frame.size.width,  size.height+20+100-(scrollview.frame.size.height)+10) animated:NO];
    }
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}



- (void) registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    
}

- (void) keyboardWasShown:(NSNotification *) notif
{
    NSDictionary *info = [notif userInfo];
    NSValue *value = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGSize keyboardSize = [value CGRectValue].size;
    textviewboardheight = keyboardSize.height;
    
    UIImage* imageback = [UIImage imageNamed:@"backviewreview"];

    if([self.myView viewWithTag:7618])
    {

        UIImageView* imageview = (UIImageView*)[self.myView viewWithTag:7618];
        imageview.frame = CGRectMake(0, self.myView.frame.size.height-textviewboardheight-imageback.size.height, self.myView.frame.size.width, imageback.size.height);
    }
    else
    {
        [self textviewbuttonreview];

    }
    
    maxtextviewheight = self.myView.frame.size.height-textviewboardheight-imageback.size.height-22;
    
    scrollview.frame = CGRectMake(0, 0, self.myView.frame.size.width, self.myView.frame.size.height-textviewboardheight-imageback.size.height);
    scrollview.contentSize = CGSizeMake(scrollview.frame.size.width, scrollview.frame.size.height);
    [self textViewDidBeginEditing:textview];
    

}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    if(faceimageview)
    {
        [faceimageview removeFromSuperview];
        faceimageview = nil;
    }

}


-(void)removetextviewbuttonreview //释放照相机试图
{
    [[self.myView viewWithTag:7618] removeFromSuperview];
}






-(BOOL)ispassupdate//验证可否通过要求
{
    BOOL ispass = YES;
    if(![dicparm objectForKey:@"4"])
    {
        ispass = NO;
        [self showAlertView:@"温馨提示" subtitle:@"请输入评论内容"];
    }
    return ispass;

}



- (void)textViewDidBeginEditing:(UITextView *)textView
{

    if([textview.text isEqualToString:@"请输入评论内容……"])
    {
        textview.text = @"";
    }
    
    imageviewphoto.hidden = YES;
    UILabel* label = nil;
    textview.scrollEnabled = YES;//是否可以拖动
    [self.myView viewWithTag:9123].hidden = NO;
    if([scrollview viewWithTag:1098])
    {
        label = (UILabel*)[scrollview viewWithTag:1098];
    }
    else
    {
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        label.textColor = [UIColor blackColor];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentRight;
        label.font = [UIFont systemFontOfSize:12];
        label.tag = 1098;
        [scrollview addSubview:label];//字数限制
    }
    if(scrollview.frame.origin.y!=0)
    {
        textviewy = scrollview.frame.origin.y;
    }
    
    CGSize size = CGSizeMake(0, 0);
    if([textview.text length])
    {
        size = [textview.text sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(textview.frame.size.width-10, 999) lineBreakMode:UILineBreakModeWordWrap];
    }
    textview.frame = CGRectMake(0, 0, scrollview.frame.size.width, maxtextviewheight);
    if(size.height>maxtextviewheight)
    {
        [textview setContentOffset:CGPointMake(0, size.height-maxtextviewheight+10) animated:NO];
        size.height = maxtextviewheight;
    }
    label.frame = CGRectMake(0, size.height+10, textview.frame.size.width, 12);
    label.text = [NSString stringWithFormat:@"%d/500",[textview.text length]];
    

}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    if(faceimageview)
    {
        [faceimageview removeFromSuperview];
        faceimageview = nil;
    }
    if([textview.text isEqualToString:@""])
    {
        textview.text = @"请输入评论内容……";
        [dicparm removeObjectForKey:@"4"];//移除评论文字

    }
    else
    {
        [dicparm setObject:textview.text forKey:@"4"];//添加评论文字
    }

    [self.myView viewWithTag:9123].hidden = YES;
    [[scrollview viewWithTag:1098] removeFromSuperview];
    [self removetextviewbuttonreview];
    imageviewphoto.hidden = NO;
    textview.scrollEnabled = NO;//是否可以拖动
    
    CGSize size = [textview.text sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(scrollview.frame.size.width-10, 999) lineBreakMode:NSLineBreakByWordWrapping];
    
    if(imageviewphoto)
    {
        imageviewphoto.frame = CGRectMake(5, size.height+20, 100, 100);
    }
    
    scrollview.frame = CGRectMake(0, textviewy, self.myView.frame.size.width,self.myView.frame.size.height-textviewy);
    textview.frame = CGRectMake(0, 0, scrollview.frame.size.width, scrollview.frame.size.height);
    if(imageviewphoto)
    {
        imageviewphoto.frame = CGRectMake(5, size.height+20, 100, 100);
        scrollview.contentSize = CGSizeMake(scrollview.frame.size.width,size.height+20+100);

    }
    else
    {
        scrollview.contentSize = CGSizeMake(scrollview.frame.size.width, size.height);

    }
    
    
}

-(BOOL) textView :(UITextView *) textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *) text
{
    if ([text isEqualToString:@"\n"])
    {
        [textview resignFirstResponder];
    }
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView
{
    
    CGSize size = [textview.text sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(textview.frame.size.width-10, 999) lineBreakMode:UILineBreakModeWordWrap];
    
    UILabel*label = (UILabel*)[scrollview viewWithTag:1098];
    if(size.height>maxtextviewheight)
    {
        size.height = maxtextviewheight;
    }
    label.frame = CGRectMake(0, size.height+10, textview.frame.size.width, 12);
    label.text = [NSString stringWithFormat:@"%d/500",[textview.text length]];
    
    
    if(textview.text.length>500)
    {
        [self showAlertView:@"温馨提示" subtitle:@"点评内容最多只可输入500字"];
    }
    else
    {
        ;
    }
    
}




- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setTitle:@"写评论"];
    [self addNavBarButton:RIGHT_SUBMENT];
    textviewtextheight = 0;
    arraystar = [[NSMutableArray alloc] init];
    dicparm = [[NSMutableDictionary alloc] init];
    [dicparm setObject:[NSNumber numberWithInt:3] forKey:@"0"];
    [dicparm setObject:[NSNumber numberWithInt:3] forKey:@"1"];
    [dicparm setObject:[NSNumber numberWithInt:3] forKey:@"2"];
    [dicparm setObject:[NSNumber numberWithInt:3] forKey:@"3"];

    self.myView.tag = 9832;
    [self initviewstyle];
    self.myView.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view from its nib.
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerForKeyboardNotifications];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event //点击输入框背景取消输入框
{
    UITouch* touch = [touches anyObject];
    if(([touch.view isKindOfClass:[textview class]]||[touch.view isKindOfClass:[UIButton class]]))
    {
        ;
    }
    else
    {
        [textview resignFirstResponder];

    }
    
    int x  = [touch locationInView:touch.view].x;
    int y = [touch locationInView:touch.view].y;
   

    if((touch.view.tag == 9832)&&(y<=starty)&&(y>0))
    {
       
        [self drawstarreview:x indexy:y];
    }
    

}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch = [touches anyObject];

    int x  = [touch locationInView:touch.view].x;
    int y = [touch locationInView:touch.view].y;
    if((touch.view.tag == 9832)&&(y<=starty)&&(y>0))
    {
        [self drawstarreview:x indexy:y];
        
    }



}

-(void)drawstarreview:(int)x indexy:(int)y //评论星星
{
    NSLog(@"%d,%d",x,y);
    int indexy = -1; //纵坐标
    int indexx = -1; //heng坐标
    for(int i = 0 ; i < [arraystar count];i++) //找y
    {
        
        NSArray* array = [arraystar objectAtIndex:i];
        int stary = [[[array objectAtIndex:0] objectForKey:@"stary"] intValue];
        if(y<=stary)
        {
            indexy = i;
            break;
        }
    }
    NSArray* array;
    if(indexy>=0)
    {
       array = [arraystar objectAtIndex:indexy];
    }
    if(array)
    {
        for(int i = 0 ; i < [array count];i++) //找x
        {
            
            NSDictionary* dic = [array objectAtIndex:i];
            int starx = [[dic objectForKey:@"starx"] intValue];
            if(x<=starx)
            {
                indexx = i;
                break;
            }
        }
        
        NSLog(@"%d,%d",indexy,indexx);
        
        if(indexx>=0)
        {
            UIImage* upimage = [UIImage imageNamed:@"productreviewsup"];
            UIImage* downimage = [UIImage imageNamed:@"productreviewsdown"];
            for(int i = 0 ; i < 5 ; i++) //花星星
            {
                
                NSDictionary* dic = [array objectAtIndex:i];
                int tag = [[dic objectForKey:@"startag"] intValue];
                UIImageView* imageview = (UIImageView*)[self.myView viewWithTag:tag];
                if(i<= indexx)
                {
                    imageview.image = upimage;
                }
                else
                {
                    imageview.image = downimage;
                }
            }
            
            [dicparm setObject:[NSNumber numberWithInt:indexx] forKey:[NSString stringWithFormat:@"%d",indexy]];
            
        }
      
    }
    
}


-(void)submentbuttondown:(UIButton*)button //发布
{
    BOOL ispass = [self ispassupdate];
    NSLog(@"%d,%@",ispass,dicparm);
}

@end
