//
//  ProductReviewViewController.h
//  cherryproduct
//
//  Created by umessage on 14-8-18.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIfaceimageview.h"

@interface ProductReviewViewController : BaseViewController<UITextViewDelegate,UIImagePickerControllerDelegate,UIfaceimageviewdelegate>
{
    int textviewtextheight; //输入框文字高度
    int textviewboardheight; //键盘高度
    int maxtextviewheight; //再大textview的高
    int textviewy;//输入框y值
    int starty ;//可点击的范围
    UITextView* textview; //输入框
    NSMutableArray* arraystar; //星星的信息
    NSMutableDictionary* dicparm; //页面选择的参数
    UIImageView* imageviewphoto; //照相机的图片
    UIScrollView* scrollview;
    UIfaceimageview* faceimageview;
}
@end
