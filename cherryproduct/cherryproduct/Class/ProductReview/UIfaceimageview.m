//
//  UIfaceimageview.m
//  cherryproduct
//
//  Created by umessage on 14-8-27.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "UIfaceimageview.h"

@implementation UIfaceimageview
@synthesize delegate;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
        [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
        //注册
        UICollectionView* collectionview = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:flowLayout];
        [collectionview registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
        collectionview.delegate = self;
        collectionview.dataSource  = self;
        collectionview.backgroundColor = [UIColor whiteColor];
        [self addSubview:collectionview]; //表情列表

    }
    return self;
}


#pragma mark -- UICollectionViewDataSource
//定义展示的UICollectionViewCell的个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 135;
}
//定义展示的Section的个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
//每个UICollectionView展示的内容
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* string = @"cell";
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:string forIndexPath:indexPath];
    
    if([cell viewWithTag:101])
    {
        UIImageView* imageview = (UIImageView*)[cell viewWithTag:101];
        imageview.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d.gif",indexPath.row]];
    }
    else
    {
        UIImageView* imageview= [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width/7, self.frame.size.width/7)];
        imageview.image =[UIImage imageNamed:[NSString stringWithFormat:@"%d.gif",indexPath.row]];
        [cell addSubview:imageview];
    }
    cell.backgroundColor = [UIColor whiteColor];
    return cell;
}


#pragma mark --UICollectionViewDelegateFlowLayout
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.frame.size.width/7, self.frame.size.width/7);
}
//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0,0);
}
#pragma mark --UICollectionViewDelegate
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.delegate UIfaceimageviewdelegate:indexPath.row];
    NSLog(@"face%d",indexPath.row);
    
}
//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
