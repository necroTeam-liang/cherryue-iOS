//
//  BuyProductViewController.m
//  cherryproduct
//
//  Created by umessage on 14-8-18.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "BuyProductViewController.h"
#import "ProductReviewViewController.h"

@interface BuyProductViewController ()

@end

@implementation BuyProductViewController
@synthesize stringid;
@synthesize stringurl;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setTitle:@"商品详情"];
    [self addNavBarButton:RIGHT_SHARE_MESSAGE];
    webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.myView.frame.size.width, self.myView.frame.size.height)];
    NSURLRequest *request =[NSURLRequest requestWithURL:[NSURL URLWithString:self.stringurl]];
    [self.myView addSubview:webview];
    [webview loadRequest:request];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)messageButtonAction:(UIButton*)button
{
    ProductReviewViewController* review = [[ProductReviewViewController alloc] init];
    [self.navigationController pushViewController:review animated:YES];
    NSLog(@"message");
}


-(void)shareButtonAction:(UIButton*)button
{
    [self umengShare:@"樱桃" icon:nil];

    NSLog(@"share");
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
