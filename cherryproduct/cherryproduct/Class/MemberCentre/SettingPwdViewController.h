//
//  SettingPwdViewController.h
//  cherryproduct
//
//  Created by Enter zhao on 14-8-19.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "BaseViewController.h"

@interface SettingPwdViewController : BaseViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *pwdField;
@property (strong, nonatomic) IBOutlet UITextField *rePwdField;
- (IBAction)okAction:(id)sender;

@end
