//
//  MemberCentreViewController.h
//  cherryproduct
//
//  Created by Enter zhao on 14-8-12.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "BaseViewController.h"
#import "MemberModel.h"
@interface MemberCentreViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSDictionary* mainDic;
    
    NSMutableArray* mainArray;
    UIImageView* coverImageView;
    UIImageView* logoImageView;
}
@end
