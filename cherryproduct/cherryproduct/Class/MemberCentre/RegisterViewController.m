//
//  RegisterViewController.m
//  cherryproduct
//
//  Created by Enter zhao on 14-8-18.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setTitle:@"注册"];
    i = NO;
    self.myView.hidden = YES;
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)brackButtonAction:(id)sender {
}

- (IBAction)okButtonAction:(id)sender {
    if (i == NO) {
        [self showAlertView:@"温馨提示" subtitle:@"您还没有同意用户协议哦~"];
        return;
    }
    NSMutableDictionary *requestDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                       _emailTield.text, @"email",_pwdTield.text, @"pwd",_rePwdTield.text,@"repwd",nil];
    [[HttpNewBaseService sharedClient] postRequestOperation:@"member/register" paramDictionary:requestDic onCompletion:^(NSString *responseString) {
        @try {
//            [self showAlertView:@"温馨提示" subtitle:@"注册成功"];
            __block URBAlertView *alertView = [URBAlertView dialogWithTitle:@"温馨提示" subtitle:@"注册成功"];
            alertView.blurBackground = YES;
            //	[alertView addButtonWithTitle:@"Close"];
            [alertView addButtonWithTitle:@"确定"];
            [alertView setHandlerBlock:^(NSInteger buttonIndex, URBAlertView *alertView) {
                NSLog(@"button tapped: index=%i", buttonIndex);
                [alertView hideWithCompletionBlock:^{
                    // stub
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }];
            [alertView showWithAnimation:URBAlertAnimationFlipHorizontal];
        }
        @catch (NSException *exception) {
            [self showAlertView:@"温馨提示" subtitle:@"网络通讯失败！"];
            
        }
        @finally {
            ;
        }
        
    } onError:^(NSError *error) {
        ;
    } onAnimated:YES];
}

- (IBAction)userProtocolAction:(id)sender {

    i = !i;
    if (i) {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"用户协议" message:@"用户协议用户协议用户协议用户协议用户协议用户协议用户协议用户协议用户协议用户协议用户协议" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        _protocol.backgroundColor = [UIColor blueColor];
    }
    else
    {
                _protocol.backgroundColor = [UIColor whiteColor];
    }
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSTimeInterval animationDuration = 0.30f;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    CGRect rect = CGRectMake(0.0f,0 , self.view.frame.size.width, self.view.frame.size.height);
    self.view.frame = rect;
    [UIView commitAnimations];
    [_rePwdTield resignFirstResponder];
    [_emailTield resignFirstResponder];
    [_pwdTield resignFirstResponder];
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    NSTimeInterval animationDuration = 0.30f;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    CGRect rect = CGRectMake(0.0f,0 , self.view.frame.size.width, self.view.frame.size.height);
    self.view.frame = rect;
    [UIView commitAnimations];
    [textField resignFirstResponder];
    
    return YES;
    
}
@end
