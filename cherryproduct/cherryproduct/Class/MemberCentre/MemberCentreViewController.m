//
//  MemberCentreViewController.m
//  cherryproduct
//
//  Created by Enter zhao on 14-8-12.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "MemberCentreViewController.h"
#import "ChangeMessageViewController.h"
#import "HttpNewBaseService.h"
#import "UIImageView+AFNetworking.h"


@interface MemberCentreViewController ()

@end

@implementation MemberCentreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    //背景图片
    coverImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"titleBackground"]];
    coverImageView.userInteractionEnabled = YES;
    coverImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 164);

    
    //    logo头像
    logoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 88, 70, 70)];
    logoImageView.userInteractionEnabled = YES;
    logoImageView.image = [UIImage imageNamed:@"mainviewfuturebuttondown"];
    logoImageView.layer.cornerRadius = 35;
    logoImageView.layer.masksToBounds = YES;
    logoImageView.layer.borderWidth = 3.0;
    logoImageView.layer.borderColor = [UIColor whiteColor].CGColor;

    [coverImageView addSubview:logoImageView];
    
    [self.view addSubview:coverImageView];

    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"个人中心"];
    self.myView.hidden = YES;
    [self addNavBarButton:RIGHT_SETTING];
    [self setNavigationBarColor:[UIColor clearColor]];
    
    UITableView* tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 165, SCREEN_WIDTH, SCREEN_HEIGHT-164)];
    tableView.dataSource = self;
    tableView.delegate = self;
//    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;

    [self.view addSubview:tableView];
    
    [[HttpNewBaseService sharedClient] postRequestOperation:@"member/feed" paramDictionary:nil onCompletion:^(NSString *responseString) {
        @try {
            
            NSDictionary* dicjson = [responseString JSONValue];
            if ([[dicjson objectForKey:@"status"] intValue] == 1) {
                mainDic = [[NSDictionary alloc] initWithDictionary:dicjson[@"data"][@"record_type"] copyItems:YES];
                mainArray = [[NSMutableArray alloc] init];
                NSArray* array = dicjson[@"data"][@"feed"];
                for (NSDictionary* dic in array) {
                    MemberModel* model = [[MemberModel alloc] init];
                    model.feedId = dic[@"id"];
                    model.fid = dic[@"fid"];
                    model.mid = dic[@"mid"];
                    model.type = dic[@"type"];
                    model.addtime = dic[@"addtime"];
                    model.updatetime = dic[@"updatetime"];
                    model.alarm_time = dic[@"alarm_time"];
                    model.status = dic[@"status"];
                    model.title = dic[@"title"];
                    model.hour = dic[@"hour"];
                    model.day = dic[@"day"];
                    for (int i = 0; i<[model.title length]; i++) {
                        //截取字符串中的每一个字符
                        NSString *s = [model.title substringWithRange:NSMakeRange(i, 1)];
                        NSLog(@"string is %@",s);
                        if ([s isEqualToString:@"#"]) {
                            NSRange range = NSMakeRange(i, 1);
                            model.title =   [model.title stringByReplacingCharactersInRange:range withString:@""];
                            
                        }
                    }

                    
                    model.goodsArray = [[NSMutableArray alloc] init];
                    
                    if ([dic objectForKey:@"goods"]) {
                        GoodsModel* mGoods = [[GoodsModel alloc] init];
                        mGoods.name = dic[@"goods"][@"name"];
                        mGoods.start_time = dic[@"goods"][@"start_time"];
                        mGoods.end_time = dic[@"goods"][@"end_time"];
                        mGoods.status = dic[@"goods"][@"status"];
                        mGoods.cid = dic[@"goods"][@"cid"];
                        mGoods.photo = dic[@"goods"][@"photo"];
                        mGoods.buy_num = dic[@"goods"][@"buy_num"];
                        mGoods.apply_num = dic[@"goods"][@"apply_num"];
                        mGoods.price = dic[@"goods"][@"price"];
                        mGoods.des = dic[@"goods"][@"des"];
                        mGoods.sort = dic[@"goods"][@"sort"];
                        mGoods.color = dic[@"goods"][@"color"];
                        mGoods.addtime = dic[@"goods"][@"addtime"];
                        mGoods.updatetime = dic[@"goods"][@"updatetime"];
                        [model.goodsArray addObject:mGoods];
                    }
                    else
                    {
                        NSArray* arr = [dic objectForKey:@"groupon"];
                        for (NSDictionary* dic1 in arr) {
                            NSDictionary* dic = dic1[@"goods"];
                            GoodsModel* mGoods = [[GoodsModel alloc] init];
                            mGoods.name = dic[@"name"];
                            
                            mGoods.start_time = dic[@"start_time"];
                            mGoods.end_time = dic[@"end_time"];
                            mGoods.status = dic[@"status"];
                            mGoods.cid = dic[@"cid"];
                            mGoods.photo = dic[@"photo"];
                            mGoods.buy_num = dic[@"buy_num"];
                            mGoods.apply_num = dic[@"apply_num"];
                            mGoods.price = dic[@"price"];
                            mGoods.des = dic[@"des"];
                            mGoods.sort = dic[@"sort"];
                            mGoods.color = dic[@"color"];
                            mGoods.addtime = dic[@"addtime"];
                            mGoods.updatetime = dic[@"updatetime"];
                            [model.goodsArray addObject:mGoods];
                        }
                    }
                    [mainArray addObject:model];
                }
                [tableView reloadData];
                
            }
            else
            {
                [self showAlertView:@"温馨提示" subtitle:[dicjson objectForKey:@"message"]];
            }
        }
        @catch (NSException *exception) {
            [self showAlertView:@"温馨提示" subtitle:@"网络通讯失败！"];
        }
        @finally {
            ;
        }
        
    } onError:^(NSError *error) {
        ;
    } onAnimated:YES];
    [self getUserData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)settingButtonAction:(UIButton *)sender
{
    ChangeMessageViewController* vc = [[ChangeMessageViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)getUserData
{
    [[HttpNewBaseService sharedClient] getRequestOperation:@"member/get_profile" paramDictionary:nil onCompletion:^(NSString *responseString) {
        @try {
            
            NSDictionary* dic =[responseString JSONValue];
            if ([[dic objectForKey:@"status"] intValue] == 1) {

                
                NSMutableString* stringimageurl = [[NSMutableString alloc] init];
                [stringimageurl appendString:JPG];
                [stringimageurl appendString:[[dic objectForKey:@"data"] objectForKey:@"cover"]];
                [coverImageView setImageWithURL:[NSURL URLWithString:stringimageurl] placeholderImage:nil];
                
                NSMutableString* stringimageurl1 = [[NSMutableString alloc] init];
                [stringimageurl1 appendString:JPG];
                [stringimageurl1 appendString:[[dic objectForKey:@"data"] objectForKey:@"logo"]];
                [logoImageView setImageWithURL:[NSURL URLWithString:stringimageurl1] placeholderImage:nil];
                
            }
            else
            {
                [self showAlertView:@"温馨提示" subtitle:[dic objectForKey:@"message"]];
            }
        }
        @catch (NSException *exception) {
            [self showAlertView:@"温馨提示" subtitle:@"网络通讯失败！"];
        }
        @finally {
            ;
        }
        
    } onError:^(NSError *error) {
        ;
    } onAnimated:YES];
}
#pragma -mark tableView
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [[UITableViewCell alloc] init];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    cell.backgroundColor = [UIColor colorWithRed:249.0/255.0 green:249.0/255.0 blue:249.0/255.0 alpha:1.0];
    cell.backgroundColor = [UIColor whiteColor];
    UIView* timeLine = [[UIView alloc] initWithFrame:CGRectMake(52, 0, 2, 151)];
    timeLine.backgroundColor = [UIColor colorWithRed:217.0/255.0 green:206.0/255.0 blue:189.0/255.0 alpha:1.0];
    [cell addSubview:timeLine];
    


    
    MemberModel* model = [mainArray objectAtIndex:indexPath.row];
    int i = 0;
    for (GoodsModel* temp in model.goodsArray) {
        UIImageView* goodsView = [[UIImageView alloc] initWithFrame:CGRectMake(75+i*85, 35, 75, 75)];
        NSMutableString* stringimageurl = [[NSMutableString alloc] init];
        [stringimageurl appendString:JPG];
        [stringimageurl appendString:temp.photo];
        [goodsView setImageWithURL:[NSURL URLWithString:stringimageurl] placeholderImage:nil];
        [cell addSubview:goodsView];
        
        UILabel* name = [UILabel initWithCommonParameters:CGRectMake(78+i*85, 105, 75, 30) text:temp.name textColor:[UIColor grayColor] fontSize:11 isNewLine:NO];
        [cell addSubview:name];
        i++;
    }
    
    if (model) {
        NSRange range=[model.title rangeOfString:@"\""];//指定的字符串从左往右匹配（系统默认）。
        NSLog(@"%@",NSStringFromRange(range));
        //指定的字符串从右往左匹配
        NSRange range1=[model.title rangeOfString:@"\"" options:NSBackwardsSearch];
        NSLog(@"%@",NSStringFromRange(range1));
        
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:model.title];
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:43.0/255.0 green:124.0/255.0 blue:246.0/255.0 alpha:1.0] range:NSMakeRange(range.location+1, range1.location-range.location-1)];
        
        
        UILabel* title = [UILabel initWithCommonParameters:CGRectMake(75, 3, 300, 30) text:model.title textColor:[UIColor grayColor] fontSize:14 isNewLine:NO];
        title.attributedText = str;
        [cell addSubview:title];
        
        NSString* data = [NSString stringWithFormat:@"%@\n%@",model.hour,model.day];
        UILabel* dataLabel = [UILabel initWithCommonParameters:CGRectMake(5, 45, 60, 90) text:data textColor:[UIColor grayColor] fontSize:12 isNewLine:YES];
        [cell addSubview:dataLabel];
    }

    
    UIImageView* searchImage = [[UIImageView alloc] initWithFrame:CGRectMake(52-15, 8, 30, 30)];
    searchImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d",indexPath.row]];

    [cell addSubview:searchImage];
    return cell;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MemberModel* model = [mainArray objectAtIndex:indexPath.row];
    if ([model.feedId isEqualToString:@"1"]) {
     //团购
    }
    else if ([model.feedId isEqualToString:@"2"]) {
//    团购
    }
    else if ([model.feedId isEqualToString:@"3"]) {
//        商品详情
    }
    else if ([model.feedId isEqualToString:@"4"]) {
//        点评
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
