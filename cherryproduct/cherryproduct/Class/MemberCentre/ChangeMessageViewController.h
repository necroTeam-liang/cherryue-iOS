//
//  ChangeMessageViewController.h
//  cherryproduct
//
//  Created by Enter zhao on 14-8-13.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "BaseViewController.h"

@interface ChangeMessageViewController : BaseViewController<UIScrollViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    UIView* changePhotoView;
    UIImageView* coverImageView;
    UIImageView* logoImageView;
    BOOL isCover;
    BOOL isLogo;
    int _month;
    int _year;
    
}

@property (strong, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) NSDictionary* selfMessage;
- (IBAction)saveButtonAction:(UIButton *)sender;
- (IBAction)logoutButtonAction:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *signLabel;
@property (strong, nonatomic) IBOutlet UILabel *ageLabel;
@property (strong, nonatomic) IBOutlet UILabel *ringLabel;
- (IBAction)changeNameAction:(UIButton *)sender;
- (IBAction)changeSignAction:(UIButton *)sender;
- (IBAction)changeAgeAction:(UIButton *)sender;
- (IBAction)changeRingAciton:(UIButton *)sender;
//- (void)
@end
