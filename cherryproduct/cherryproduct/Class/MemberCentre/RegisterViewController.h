//
//  RegisterViewController.h
//  cherryproduct
//
//  Created by Enter zhao on 14-8-18.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "BaseViewController.h"

@interface RegisterViewController : BaseViewController<UITextFieldDelegate>
{
    BOOL i;
}
- (IBAction)brackButtonAction:(id)sender;
- (IBAction)okButtonAction:(id)sender;
- (IBAction)userProtocolAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *protocol;
@property (strong, nonatomic) IBOutlet UITextField *emailTield;
@property (strong, nonatomic) IBOutlet UITextField *pwdTield;
@property (strong, nonatomic) IBOutlet UITextField *rePwdTield;

@end
