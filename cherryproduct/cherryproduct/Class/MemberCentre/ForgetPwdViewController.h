//
//  ForgetPwdViewController.h
//  cherryproduct
//
//  Created by Enter zhao on 14-8-19.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "BaseViewController.h"

@interface ForgetPwdViewController : BaseViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *emailField;
@property (strong, nonatomic) IBOutlet UITextField *signField;
- (IBAction)sendEmailAction:(id)sender;
- (IBAction)nextAction:(id)sender;
@end
