//
//  babyMessageViewController.h
//  cherryproduct
//
//  Created by Enter zhao on 14-8-18.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface babyMessageViewController : BaseViewController
{
    NSString* _year;
    NSString* _month;
    UILabel* _tempLabel;
}
@property (assign, nonatomic) BOOL sex;

@property (strong, nonatomic) IBOutlet UIView *segementView;
@property (strong, nonatomic) IBOutlet UIButton *boyButton;
@property (strong, nonatomic) IBOutlet UIButton *girlButton;
@property (strong, nonatomic) IBOutlet UILabel *yearLabel;
@property (strong, nonatomic) IBOutlet UILabel *monthLabel;
- (IBAction)yearDown:(UIButton *)sender;
- (IBAction)yearUp:(UIButton *)sender;
- (IBAction)monthDown:(UIButton *)sender;
- (IBAction)monthUp:(UIButton *)sender;
- (IBAction)boyButtonAction:(UIButton *)sender;
- (IBAction)girlButtonAction:(UIButton *)sender;
- (IBAction)okButtonAction:(UIButton *)sender;
- (void)setYearAndmonth:(int)year month:(int)nonth babyLabel:(UILabel*)label;
@end
