//
//  SearchViewController.h
//  cherryproduct
//
//  Created by Enter zhao on 14-8-10.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "BaseViewController.h"

@interface SearchViewController : BaseViewController<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)UISearchBar* serchBar;
@property(nonatomic,strong)NSMutableArray* possibleItems;
@property(nonatomic,strong)NSMutableArray* searchArray;
@property(nonatomic,strong)UITableView* searchTableView;
@end
