//
//  LoginViewController.h
//  cherryproduct
//
//  Created by Enter zhao on 14-8-6.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "BaseViewController.h"
#import "WeiboSDK.h"
#import <TencentOpenAPI/TencentOAuth.h>
@interface LoginViewController : BaseViewController<WBHttpRequestDelegate,TencentSessionDelegate,UITextFieldDelegate>
{
    NSMutableArray* _permissions;
    TencentOAuth* _tencentOAuth;
}
@property (strong, nonatomic) IBOutlet UITextField *userNameField;
@property (strong, nonatomic) IBOutlet UITextField *passwordField;

- (IBAction)qqbuttonAction:(UIButton *)sender;
- (IBAction)sinaButtonAction:(UIButton *)sender;
- (IBAction)forgetPwdButtonAction:(UIButton *)sender;
- (IBAction)loginButtonAction:(UIButton *)sender;
- (IBAction)enterButtonAction:(UIButton *)sender;

@end
