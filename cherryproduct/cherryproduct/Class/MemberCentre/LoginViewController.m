//
//  LoginViewController.m
//  cherryproduct
//
//  Created by Enter zhao on 14-8-6.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "LoginViewController.h"

#import "HttpNewBaseService.h"
#import "MemberCentreViewController.h"
#import "RegisterViewController.h"
#import "ForgetPwdViewController.h"
@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.myView.hidden = YES;
    // Do any additional setup after loading the view from its nib.
    
    _userNameField.delegate = self;
    _passwordField.delegate = self;
    

//    NSArray* array = [[NSArray alloc] initWithObjects:[self.view viewWithTag:11000],[self.view viewWithTag:11001], nil];
//    [UIView addAnimationArrayAndPlay:array delay:0.3f playType:PLAY_LIFT_RIGHT];
//    NSArray* array1 = [[NSArray alloc] initWithObjects:[self.view viewWithTag:11002], nil];
//    [UIView addAnimationArrayAndPlay:array1 delay:0.8f playType:PLAY_RIGHT_LIFT];
    
    
    [self.navigationBar setBackgroundColor:[UIColor clearColor]];

    _permissions = [NSArray arrayWithObjects:
                     kOPEN_PERMISSION_GET_USER_INFO,
                     kOPEN_PERMISSION_GET_SIMPLE_USER_INFO,
                     kOPEN_PERMISSION_ADD_ALBUM,
                     kOPEN_PERMISSION_ADD_IDOL,
                     kOPEN_PERMISSION_ADD_ONE_BLOG,
                     kOPEN_PERMISSION_ADD_PIC_T,
                     kOPEN_PERMISSION_ADD_SHARE,
                     kOPEN_PERMISSION_ADD_TOPIC,
                     kOPEN_PERMISSION_CHECK_PAGE_FANS,
                     kOPEN_PERMISSION_DEL_IDOL,
                     kOPEN_PERMISSION_DEL_T,
                     kOPEN_PERMISSION_GET_FANSLIST,
                     kOPEN_PERMISSION_GET_IDOLLIST,
                     kOPEN_PERMISSION_GET_INFO,
                     kOPEN_PERMISSION_GET_OTHER_INFO,
                     kOPEN_PERMISSION_GET_REPOST_LIST,
                     kOPEN_PERMISSION_LIST_ALBUM,
                     kOPEN_PERMISSION_UPLOAD_PIC,
                     kOPEN_PERMISSION_GET_VIP_INFO,
                     kOPEN_PERMISSION_GET_VIP_RICH_INFO,
                     kOPEN_PERMISSION_GET_INTIMATE_FRIENDS_WEIBO,
                     kOPEN_PERMISSION_MATCH_NICK_TIPS_WEIBO,
                     nil];
	
    NSString *appid = @"222222";
    
	_tencentOAuth = [[TencentOAuth alloc] initWithAppId:appid
											andDelegate:self];
    


}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];


}
- (void)request:(WBHttpRequest *)request didFinishLoadingWithResult:(NSString *)result
{
    NSString *title = nil;
    UIAlertView *alert = nil;
    
    title = @"收到网络回调";
    alert = [[UIAlertView alloc] initWithTitle:title
                                       message:[NSString stringWithFormat:@"%@",result]
                                      delegate:nil
                             cancelButtonTitle:@"确定"
                             otherButtonTitles:nil];
    [alert show];
}

- (void)request:(WBHttpRequest *)request didFailWithError:(NSError *)error;
{
    NSString *title = nil;
    UIAlertView *alert = nil;
    
    title = @"请求异常";
    alert = [[UIAlertView alloc] initWithTitle:title
                                       message:[NSString stringWithFormat:@"%@",error]
                                      delegate:nil
                             cancelButtonTitle:@"确定"
                             otherButtonTitles:nil];
    [alert show];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSTimeInterval animationDuration = 0.30f;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    CGRect rect = CGRectMake(0.0f,0 , self.view.frame.size.width, self.view.frame.size.height);
    self.view.frame = rect;
    [UIView commitAnimations];
    [_passwordField resignFirstResponder];
    [_userNameField resignFirstResponder];
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    NSTimeInterval animationDuration = 0.30f;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    CGRect rect = CGRectMake(0.0f,0 , self.view.frame.size.width, self.view.frame.size.height);
    self.view.frame = rect;
    [UIView commitAnimations];
    [textField resignFirstResponder];
    
    return YES;
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)qqbuttonAction:(UIButton *)sender {
    [_tencentOAuth authorize:_permissions inSafari:NO];
}

- (IBAction)sinaButtonAction:(UIButton *)sender {
    WBAuthorizeRequest *request = [WBAuthorizeRequest request];
    request.redirectURI = kRedirectURI;
    request.scope = @"all";
    request.userInfo = @{@"SSO_From": @"LoginViewController",
                         @"Other_Info_1": [NSNumber numberWithInt:123],
                         @"Other_Info_2": @[@"obj1", @"obj2"],
                         @"Other_Info_3": @{@"key1": @"obj1", @"key2": @"obj2"}};
    [WeiboSDK sendRequest:request];
}

- (IBAction)forgetPwdButtonAction:(UIButton *)sender {
    ForgetPwdViewController* vc = [[ForgetPwdViewController alloc] initWithNibName:@"ForgetPwdViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)loginButtonAction:(UIButton *)sender {

    RegisterViewController* vc = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)enterButtonAction:(UIButton *)sender {
    if (_userNameField.text.length||_passwordField.text.length) {
//        进入个人中心
        NSMutableDictionary *requestDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                           _userNameField.text, @"email",_passwordField.text, @"pwd",nil];
        [[HttpNewBaseService sharedClient] postRequestOperation:@"member/login" paramDictionary:requestDic onCompletion:^(NSString *responseString) {
            @try {
                
                NSDictionary* dic = [responseString JSONValue];
                if ([[dic objectForKey:@"status"] intValue] == 1) {
//                    登陆成功
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setObject:_userNameField.text forKey:@"USERNAME"];
                    [defaults setObject:_passwordField.text forKey:@"USERPASSWORD"];
                    [defaults synchronize];
                
                    MemberCentreViewController* vc = [[MemberCentreViewController alloc]init];
                  
                    [self.navigationController pushViewController:vc animated:YES];
                }
                else
                {
                    [self showAlertView:@"温馨提示" subtitle:[dic objectForKey:@"message"]];
                }
            }
            @catch (NSException *exception) {
                [self showAlertView:@"温馨提示" subtitle:@"网络通讯失败！"];
            }
            @finally {
                ;
            }
            
        } onError:^(NSError *error) {
            ;
        } onAnimated:YES];
    }
    else
    {
        [self showAlertView:@"温馨提示" subtitle:@"用户名密码不能为空~"];
    }
}
@end
