//
//  babyMessageViewController.m
//  cherryproduct
//
//  Created by Enter zhao on 14-8-18.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "babyMessageViewController.h"

@interface babyMessageViewController ()

@end

@implementation babyMessageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.myView.hidden = YES;
    [self setTitle:@"宝宝信息"];
    _boyButton.backgroundColor = _segementView.backgroundColor;
    [_boyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _girlButton.backgroundColor = [UIColor whiteColor];
    [_girlButton setTitleColor:_segementView.backgroundColor forState:UIControlStateNormal];
    self.view.backgroundColor = [UIColor whiteColor];
    _yearLabel.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"biankuang"]];
    _monthLabel.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"biankuang"]];
    
    _yearLabel.text = _year;
    _monthLabel.text = _month;

}

- (IBAction)yearDown:(UIButton *)sender {
    if (_yearLabel.text.intValue<0) {
        return;
    }
    _yearLabel.text = [NSString stringWithFormat:@"%d",_yearLabel.text.intValue-1];
}

- (IBAction)yearUp:(UIButton *)sender {

    _yearLabel.text = [NSString stringWithFormat:@"%d",_yearLabel.text.intValue+1];
}

- (IBAction)monthDown:(UIButton *)sender {
    if (_monthLabel.text.intValue<0) {
        return;
    }
    _monthLabel.text = [NSString stringWithFormat:@"%d",_monthLabel.text.intValue-1];
}

- (IBAction)monthUp:(UIButton *)sender {
    if (_monthLabel.text.intValue>12) {
        return;
    }
    _monthLabel.text = [NSString stringWithFormat:@"%d",_monthLabel.text.intValue+1];
}

- (IBAction)boyButtonAction:(UIButton *)sender {
    _boyButton.backgroundColor = _segementView.backgroundColor;
    [_boyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _girlButton.backgroundColor = [UIColor whiteColor];
    [_girlButton setTitleColor:_segementView.backgroundColor forState:UIControlStateNormal];
    _sex = YES;
}

- (IBAction)girlButtonAction:(UIButton *)sender {
    _girlButton.backgroundColor = _segementView.backgroundColor;
    [_girlButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _boyButton.backgroundColor = [UIColor whiteColor];
    [_boyButton setTitleColor:_segementView.backgroundColor forState:UIControlStateNormal];
    _sex = NO;
}

- (IBAction)okButtonAction:(UIButton *)sender {
    
    NSTimeInterval time = [[NSDate date] timeIntervalSince1970]-(_yearLabel.text.intValue*365+_monthLabel.text.intValue*30)*24*60*60;
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:time];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter stringFromDate:date];
    
    NSMutableDictionary* dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
               [dateFormatter stringFromDate:date],@"baby_birthday",
               _sex?@"1":@"0",@"baby_sex",nil];
    

    
    [[HttpNewBaseService sharedClient] postRequestOperation:@"member/set_profile" paramDictionary:dic onCompletion:^(NSString *responseString) {
        
        _tempLabel.text = [NSString stringWithFormat:@"%@ %@岁 %@个月",_sex?@"男":@"女",_yearLabel.text,_monthLabel.text];

    } onError:^(NSError *error) {
       
    } onAnimated:YES];
    

}
- (void)setYearAndmonth:(int)year month:(int)nonth babyLabel:(UILabel*)label
{
    _year = [NSString stringWithFormat:@"%d",year];
    _month = [NSString stringWithFormat:@"%d",nonth];
    _tempLabel = label;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
