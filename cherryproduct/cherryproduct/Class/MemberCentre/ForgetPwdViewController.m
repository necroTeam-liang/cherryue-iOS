//
//  ForgetPwdViewController.m
//  cherryproduct
//
//  Created by Enter zhao on 14-8-19.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "ForgetPwdViewController.h"
#import "SettingPwdViewController.h"

@interface ForgetPwdViewController ()

@end

@implementation ForgetPwdViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setTitle:@"密码找回"];
    self.myView.hidden = YES;
    self.view.backgroundColor = [UIColor whiteColor];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSTimeInterval animationDuration = 0.30f;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    CGRect rect = CGRectMake(0.0f,0 , self.view.frame.size.width, self.view.frame.size.height);
    self.view.frame = rect;
    [UIView commitAnimations];
    [_emailField resignFirstResponder];
    [_signField resignFirstResponder];
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    NSTimeInterval animationDuration = 0.30f;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    CGRect rect = CGRectMake(0.0f,0 , self.view.frame.size.width, self.view.frame.size.height);
    self.view.frame = rect;
    [UIView commitAnimations];
    [textField resignFirstResponder];
    
    return YES;
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sendEmailAction:(id)sender {
    NSMutableDictionary *requestDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                       _emailField.text, @"email",nil];
    [[HttpNewBaseService sharedClient] getRequestOperation:@"member/send_forgot_mail" paramDictionary:requestDic onCompletion:^(NSString *responseString) {
        @try {
            
            NSDictionary* dic = [responseString JSONValue];
            if ([[dic objectForKey:@"status"] intValue] == 1) {
                [self showAlertView:@"温馨提示" subtitle:[dic objectForKey:@"message"]];
            }
            else
            {
                [self showAlertView:@"温馨提示" subtitle:[dic objectForKey:@"message"]];
            }
        }
        @catch (NSException *exception) {
            [self showAlertView:@"温馨提示" subtitle:@"网络通讯失败！"];
        }
        @finally {
            ;
        }
        
    } onError:^(NSError *error) {
        ;
    } onAnimated:YES];
}

- (IBAction)nextAction:(id)sender {
    NSMutableDictionary *requestDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                       _signField.text, @"verify_code",nil];
    [[HttpNewBaseService sharedClient] getRequestOperation:@"member/check_verify_code" paramDictionary:requestDic onCompletion:^(NSString *responseString) {
        @try {
            
            NSDictionary* dic = [responseString JSONValue];
            if ([[dic objectForKey:@"status"] intValue] == 1) {
                SettingPwdViewController* vc = [[SettingPwdViewController alloc] initWithNibName:@"SettingPwdViewController" bundle:nil];
                [self.navigationController pushViewController:vc animated:YES];
            }
            else
            {
                [self showAlertView:@"温馨提示" subtitle:[dic objectForKey:@"message"]];
            }
        }
        @catch (NSException *exception) {
            [self showAlertView:@"温馨提示" subtitle:@"网络通讯失败！"];
        }
        @finally {
            ;
        }
        
    } onError:^(NSError *error) {
        ;
    } onAnimated:YES];
}
@end
