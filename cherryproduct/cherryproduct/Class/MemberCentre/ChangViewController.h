//
//  ChangViewController.h
//  cherryproduct
//
//  Created by Enter zhao on 14-8-13.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "BaseViewController.h"

@interface ChangViewController : BaseViewController<UITextViewDelegate>
{
    UILabel* tempLabel;
    NSString* strTitle;
}
@property (strong, nonatomic)UIView *babyAge;
@property(nonatomic,strong)UITextView* textview;
- (void)setLabel:(UILabel*)label title:(NSString*)title;
@end
