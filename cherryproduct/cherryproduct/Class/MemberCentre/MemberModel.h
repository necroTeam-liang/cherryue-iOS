//
//  MemberModel.h
//  cherryproduct
//
//  Created by Enter zhao on 14-8-19.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MemberModel : NSObject
@property(nonatomic,strong) NSString* feedId;
@property(nonatomic,strong) NSString* fid;
@property(nonatomic,strong) NSString* mid;
@property(nonatomic,strong) NSString* type;
@property(nonatomic,strong) NSString* addtime;
@property(nonatomic,strong) NSString* updatetime;
@property(nonatomic,strong) NSString* alarm_time;
@property(nonatomic,strong) NSString* status;
@property(nonatomic,strong) NSString* title;
@property(nonatomic,strong) NSString* hour;
@property(nonatomic,strong) NSString* day;

@property(nonatomic,strong) NSMutableArray* goodsArray;

@end
@interface GoodsModel : NSObject
@property(nonatomic,strong) NSString* goodId;
@property(nonatomic,strong) NSString* name;
@property(nonatomic,strong) NSString* start_time;
@property(nonatomic,strong) NSString* end_time;
@property(nonatomic,strong) NSString* status;
@property(nonatomic,strong) NSString* cid;
@property(nonatomic,strong) NSString* photo;
@property(nonatomic,strong) NSString* buy_num;
@property(nonatomic,strong) NSString* apply_num;
@property(nonatomic,strong) NSString* price;
@property(nonatomic,strong) NSString* des;
@property(nonatomic,strong) NSString* sort;
@property(nonatomic,strong) NSString* color;
@property(nonatomic,strong) NSString* addtime;
@property(nonatomic,strong) NSString* updatetime;

@end