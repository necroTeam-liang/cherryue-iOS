//
//  SettingPwdViewController.m
//  cherryproduct
//
//  Created by Enter zhao on 14-8-19.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "SettingPwdViewController.h"

@interface SettingPwdViewController ()

@end

@implementation SettingPwdViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)okAction:(id)sender {
    NSMutableDictionary *requestDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                       _pwdField.text, @"pwd",
                                       _rePwdField.text, @"re_pwd",nil];
    [[HttpNewBaseService sharedClient] getRequestOperation:@"member/reset_pwd" paramDictionary:requestDic onCompletion:^(NSString *responseString) {
        @try {
            
            NSDictionary* dic = [responseString JSONValue];
            if ([[dic objectForKey:@"status"] intValue] == 1) {
                
                __block URBAlertView *alertView = [URBAlertView dialogWithTitle:@"温馨提示" subtitle:[dic objectForKey:@"message"]];
                alertView.blurBackground = YES;
                //	[alertView addButtonWithTitle:@"Close"];
                [alertView addButtonWithTitle:@"确定"];
                [alertView setHandlerBlock:^(NSInteger buttonIndex, URBAlertView *alertView) {
                    NSLog(@"button tapped: index=%i", buttonIndex);
                    [alertView hideWithCompletionBlock:^{
                        [self popToViewController:@"LoginViewController"];
                        // stub
                    }];
                }];
                [alertView showWithAnimation:URBAlertAnimationFlipHorizontal];
                
            }
            else
            {
                [self showAlertView:@"温馨提示" subtitle:[dic objectForKey:@"message"]];
            }
        }
        @catch (NSException *exception) {
            [self showAlertView:@"温馨提示" subtitle:@"网络通讯失败！"];
        }
        @finally {
            ;
        }
        
    } onError:^(NSError *error) {
        ;
    } onAnimated:YES];
}
@end
