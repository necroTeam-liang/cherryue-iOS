//
//  ChangViewController.m
//  cherryproduct
//
//  Created by Enter zhao on 14-8-13.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "ChangViewController.h"
@interface ChangViewController ()

@end

@implementation ChangViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    _textview = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 200)];
    _textview.delegate = self;
    _textview.font = [UIFont systemFontOfSize:14];
    _textview.textColor = [UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:1.0];
    _textview.backgroundColor = [UIColor whiteColor];
    _textview.layer.borderWidth = 0.5;
    _textview.layer.borderColor = [[UIColor colorWithRed:(229.0f/255.0f) green:(229.0f/255.0f) blue:(229.0f/255.0f) alpha:1.0] CGColor];
    _textview.returnKeyType = UIReturnKeyDefault;//返回键的类型
    _textview.keyboardType = UIKeyboardTypeDefault;//键盘类型
    _textview.scrollEnabled = YES;//是否可以拖动
    _textview.delegate = self;
    _textview.userInteractionEnabled = YES;
    [self.myView addSubview:_textview];
    [self setTitle:strTitle];
    [self addNavBarButton:RIGHT_FINISH];
    
    
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setLabel:(UILabel*)label title:(NSString*)title
{
    tempLabel = label;
    strTitle = [[NSString alloc] initWithString:title];
}
- (void)finishButtonAction:(UIButton *)sender
{
    if ([_textview.text isEqualToString:@""]) {
        [self showAlertView:@"温馨提示" subtitle:@"请填写内容"];
        return;
    }
//    NSString *theImagePath = [[NSBundle mainBundle] pathForResource:@"titleBackground@2x" ofType:@"png"];
    NSMutableDictionary* dic = nil;
    if ([strTitle isEqualToString:@"昵称"]) {
        dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"2013-01-01",@"baby_birthday",
               _textview.text,@"nickname",nil];
    }
    else if ([strTitle isEqualToString:@"签名"])
    {
        dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
               _textview.text,@"sign",nil];
    }
    else if ([strTitle isEqualToString:@"宝宝信息"])
    {
        dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
               @"2013-01-01",@"baby_birthday",
               @"1",@"baby_sex",nil];
    }
    else if ([strTitle isEqualToString:@"消息提醒时间"])
    {
        dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
               _textview.text,@"warning",nil];
    }
    
    [[HttpNewBaseService sharedClient] postRequestOperation:@"member/set_profile" paramDictionary:dic onCompletion:^(NSString *responseString) {
        strTitle = nil;
        tempLabel.text = _textview.text;
        [self.navigationController popViewControllerAnimated:YES];
    } onError:^(NSError *error) {
        strTitle = nil;
        tempLabel.text = _textview.text;
        [self.navigationController popViewControllerAnimated:YES];
    } onAnimated:YES];


}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
