//
//  ClassListViewController.h
//  cherryproduct
//
//  Created by umessage on 14-8-11.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClassListViewRightClasssearchView.h"
@interface ClassListViewController : BaseViewController<UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource,ClassListViewRightClasssearchViewdelegate>
{
    BOOL isloadclass;//是否点击右侧的更多按钮
    NSMutableDictionary* dicdataparam; //分类网络请求参数
    NSMutableDictionary*dicclasslist; //分类列表数据
    UICollectionView* collectionview; 
}
@property(assign,nonatomic) BOOL issearch;//是否为搜索列表
-(void)getuseparamclasslistdata:(NSDictionary*)dic;
@end
