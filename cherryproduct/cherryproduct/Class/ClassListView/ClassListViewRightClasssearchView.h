//
//  ClassListViewRightClasssearchView.h
//  cherryproduct
//
//  Created by umessage on 14-8-12.
//  Copyright (c) 2014年 cherry. All rights reserved.
//



#import <UIKit/UIKit.h>
@protocol ClassListViewRightClasssearchViewdelegate <NSObject>

-(void)searchclassindex:(NSString*)string title:(NSString*)titlestring; //点击的分类id

@end
@interface ClassListViewRightClasssearchView : UIView<UITableViewDataSource,UITableViewDelegate>
{
    int rightflag; //点击的第几个一级分类
    UITableView* lefttableview; //二级分类
    UITableView* righttableview; //一级分类
    NSMutableArray* arraydata; //页面所需数据
}
@property(assign,nonatomic) id<ClassListViewRightClasssearchViewdelegate> delegate;
-(void)getusedata:(NSArray*)array;
@end
