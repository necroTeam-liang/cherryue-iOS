//
//  ClassListViewController.m
//  cherryproduct
//
//  Created by umessage on 14-8-11.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "ClassListViewController.h"
#import "UIImageView+AFNetworking.h"
#import "HttpNewBaseService.h"

@interface ClassListViewController ()

@end

@implementation ClassListViewController
@synthesize issearch;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        issearch = NO;
        isloadclass = NO;
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if(self.issearch)
    {
        
    }
    else //分类列表
    {
        [self addNavBarButton:RIGHT_MORE];
        
        [self setTitle:[dicdataparam objectForKey:@"title"]];
        [dicdataparam removeObjectForKey:@"title"];
        if(self.issearch)
        {
            
        }
        else //分类列表
        {
            
            [[HttpNewBaseService sharedClient] getRequestOperation:@"goods/goods_list" paramDictionary:dicdataparam onCompletion:^(NSString *responseString) {
                @try {
                    NSDictionary *jsondic = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
                    dicclasslist = [[NSMutableDictionary alloc] initWithDictionary:jsondic copyItems:YES];
                    if([jsondic objectForKey:@"data"]&&([[[jsondic objectForKey:@"data"] objectForKey:@"goods"] isKindOfClass:[NSArray class]]))
                    {
                        UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
                        [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
                        //注册
                        collectionview = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, self.myView.frame.size.width,self.myView.frame.size.height) collectionViewLayout:flowLayout];
                        [collectionview registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
                        collectionview.delegate = self;
                        collectionview.dataSource  = self;
                        collectionview.backgroundColor = [UIColor whiteColor];
                        [self.myView addSubview:collectionview]; //分类列表
                        
                    }
                    else
                    {
                        
                        [self showAlertView:@"温馨提示" subtitle:@"网络通讯失败！"];
                        
                    }
                    
                    NSLog(@"%@",responseString);
                }
                @catch (NSException *exception) {
                    [self showAlertView:@"温馨提示" subtitle:@"网络通讯失败！"];
                    
                }
                @finally {
                    ;
                }
            } onError:^(NSError *error) {
                ;
            } onAnimated:YES]; //分类列表
        }

    }
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -- UICollectionViewDataSource
//定义展示的UICollectionViewCell的个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[[dicclasslist objectForKey:@"data"] objectForKey:@"goods"] count];
}
//定义展示的Section的个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
//每个UICollectionView展示的内容
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* string = @"cell";
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:string forIndexPath:indexPath];
    
    
    if([cell viewWithTag:102])
    {
        UIImageView* imageview = (UIImageView*)[cell viewWithTag:102];
        NSString* stringurl = [NSString stringWithFormat:@"%@%@",JPG,[[[[dicclasslist objectForKey:@"data"] objectForKey:@"goods"]objectAtIndex:indexPath.row] objectForKey:@"photo"]];
        NSLog(@"%@",stringurl);
        [imageview setImageWithURL:[NSURL URLWithString:stringurl] placeholderImage:[UIImage imageNamed:@"classlistloading"]];
    }
    else
    {
        UIImageView* imageview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 130, 130)];
        imageview.tag = 102;
        NSString* stringurl = [NSString stringWithFormat:@"%@%@",JPG,[[[[dicclasslist objectForKey:@"data"] objectForKey:@"goods"] objectAtIndex:indexPath.row] objectForKey:@"photo"]];
        [imageview setImageWithURL:[NSURL URLWithString:stringurl] placeholderImage:[UIImage imageNamed:@"classlistloading"]];
        NSLog(@"%@",stringurl);

        [cell addSubview:imageview];
    }

    
    if([cell viewWithTag:101])
    {
        UILabel* label = (UILabel*)[cell viewWithTag:101];
        label.text = [[[[dicclasslist objectForKey:@"data"] objectForKey:@"goods"] objectAtIndex:indexPath.row] objectForKey:@"name"];
    }
    else
    {
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 130, 130, 40)];
        label.tag = 101;
        label.text = [[[[dicclasslist objectForKey:@"data"] objectForKey:@"goods"]objectAtIndex:indexPath.row] objectForKey:@"name"];
        label.textColor = [UIColor blackColor];
        label.textAlignment = NSTextAlignmentLeft;
        label.font = [UIFont systemFontOfSize:15];
        label.numberOfLines = 0;
        label.backgroundColor = [UIColor clearColor];
        [cell addSubview:label];
    }
    
    return cell;
}
#pragma mark --UICollectionViewDelegateFlowLayout
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(130, 170); //image130
}
//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(15, 20, 15, 20);
}
#pragma mark --UICollectionViewDelegate
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}
//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
 
}

-(void)getuseparamclasslistdata:(NSDictionary *)dic
{
    dicdataparam = [[NSMutableDictionary alloc] initWithDictionary:dic copyItems:YES];
}

-(void)moreButtonAction:(UIButton *)sender
{
    isloadclass = !isloadclass;
    if(isloadclass)
    {
        
        self.myView.frame = CGRectMake(0, self.myView.frame.origin.y, self.myView.frame.size.width+215, self.myView.frame.size.height);
        
        
        ClassListViewRightClasssearchView* rightview = [[ClassListViewRightClasssearchView alloc] initWithFrame:CGRectMake(self.myView.frame.size.width-215, 0, 215, self.myView.frame.size.height)];
        rightview.backgroundColor= [UIColor whiteColor];
        [rightview getusedata:[[dicclasslist objectForKey:@"data"] objectForKey:@"all_class"]];
        rightview.tag = 1987;
        rightview.delegate = self;
        rightview.userInteractionEnabled = YES;
        [self.myView addSubview:rightview]; //添加右侧试图
        
        UIButton* button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.myView.frame.size.width-215, self.myView.frame.size.height)];
        button.tag = 7743;
        button.backgroundColor = [UIColor blackColor];
        button.alpha = 0.0;
        [button addTarget:self action:@selector(backgounderbuttondown:) forControlEvents:UIControlEventTouchDown];
        [self.myView addSubview:button];
        [UIView animateWithDuration:0.3 animations:^{
            button.alpha = 0.5;
            self.myView.frame = CGRectMake(-215,self.myView.frame.origin.y, self.myView.frame.size.width, self.myView.frame.size.height);
        } completion:^(BOOL finished) {
            ;
        }];
        
        
        
    }
    else
    {
        [UIView animateWithDuration:0.3 animations:^{
            self.myView.frame = CGRectMake(0, self.myView.frame.origin.y, self.myView.frame.size.width-215, self.myView.frame.size.height);
            [self.myView viewWithTag:7743].alpha = 0.0;
        } completion:^(BOOL finished) {
            [[self.myView viewWithTag:7743] removeFromSuperview];
            [[self.myView viewWithTag:1987] removeFromSuperview];
        }];
    }
}
-(void)backgounderbuttondown:(UIButton*)button //遮罩点击返回原来的分类商品列表
{
    isloadclass = NO;
    [UIView animateWithDuration:0.3 animations:^{
        self.myView.frame = CGRectMake(0, self.myView.frame.origin.y, self.myView.frame.size.width-215, self.myView.frame.size.height);
        button.alpha = 0.0;
    } completion:^(BOOL finished) {
        [button removeFromSuperview];
        [[self.myView viewWithTag:1987] removeFromSuperview];

    }];

}

-(void)backButtonAction:(UIButton *)sender
{
    if(isloadclass)
    {
        isloadclass = NO;
        [UIView animateWithDuration:0.1 animations:^{
            self.myView.frame = CGRectMake(0, self.myView.frame.origin.y, self.myView.frame.size.width-215, self.myView.frame.size.height);
            [self.myView viewWithTag:7743].alpha = 0.0;
        } completion:^(BOOL finished) {
            [[self.myView viewWithTag:7743] removeFromSuperview];
            [[self.myView viewWithTag:1987] removeFromSuperview];
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)searchclassindex:(NSString *)string  title:(NSString *)titlestring//点击右侧筛选完毕的分类id
{
    [self setTitle:titlestring];
    isloadclass = NO;
    [UIView animateWithDuration:0.3 animations:^{
        self.myView.frame = CGRectMake(0, self.myView.frame.origin.y, self.myView.frame.size.width-215, self.myView.frame.size.height);
        [self.myView viewWithTag:7743].alpha = 0.0;

    } completion:^(BOOL finished) {
        [[self.myView viewWithTag:7743] removeFromSuperview];
        [[self.myView viewWithTag:1987] removeFromSuperview];
        
        if(dicdataparam)
        {
            [dicdataparam removeAllObjects];
        }
        dicdataparam = [[NSMutableDictionary alloc] init];
        [dicdataparam setObject:string forKey:@"cid"];
        [[HttpNewBaseService sharedClient] getRequestOperation:@"goods/goods_list" paramDictionary:dicdataparam onCompletion:^(NSString *responseString) {
            @try {
                NSDictionary *jsondic = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
                dicclasslist = [[NSMutableDictionary alloc] initWithDictionary:jsondic copyItems:YES];
                if([jsondic objectForKey:@"data"]&&([[[jsondic objectForKey:@"data"] objectForKey:@"goods"] isKindOfClass:[NSArray class]]))
                {
                    if(collectionview)
                    {
                        [collectionview reloadData];
                    }
                    else
                    {
                        UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
                        [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
                        //注册
                        collectionview = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, self.myView.frame.size.width,self.myView.frame.size.height) collectionViewLayout:flowLayout];
                        [collectionview registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
                        collectionview.delegate = self;
                        collectionview.dataSource  = self;
                        collectionview.backgroundColor = [UIColor whiteColor];
                        [self.myView addSubview:collectionview]; //分类列表
                        
                    }
                    
                }
                else
                {
                    
                    if(collectionview)
                    {
                        [collectionview removeFromSuperview];
                        collectionview = nil;
                    }
                    [URBAlertView dialogWithTitle:@"温馨提示" subtitle:@"网络通讯失败！"];
                    
                }
                
                NSLog(@"%@",responseString);
            }
            @catch (NSException *exception) {
                [URBAlertView dialogWithTitle:@"温馨提示" subtitle:@"网络通讯失败！"];
                
            }
            @finally {
                ;
            }
        } onError:^(NSError *error) {
            ;
        } onAnimated:YES]; //分类列表
    }];



}

@end
