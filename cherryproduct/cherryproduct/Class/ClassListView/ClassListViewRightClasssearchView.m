//
//  ClassListViewRightClasssearchView.m
//  cherryproduct
//
//  Created by umessage on 14-8-12.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "ClassListViewRightClasssearchView.h"

@implementation ClassListViewRightClasssearchView
@synthesize delegate;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        rightflag = 0;
        // Initialization code
    }
    return self;
}



-(void)getusedata:(NSArray*)array
{
    arraydata = [[NSMutableArray alloc] initWithArray:array copyItems:YES];
    
    
    lefttableview = [[UITableView alloc] initWithFrame:CGRectMake(0, 20, 100, self.frame.size.height-20) style:UITableViewStylePlain];
    lefttableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    lefttableview.userInteractionEnabled = YES;
    lefttableview.delegate = self;
    lefttableview.dataSource = self;
    lefttableview.showsVerticalScrollIndicator = NO;
    [self addSubview:lefttableview];
    
    
    righttableview = [[UITableView alloc] initWithFrame:CGRectMake(100, 5, 115, self.frame.size.height-5) style:UITableViewStylePlain];
    righttableview.userInteractionEnabled = YES;
    righttableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    righttableview.delegate = self;
    righttableview.dataSource = self;
    righttableview.showsVerticalScrollIndicator = NO;
    [self addSubview:righttableview];

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(lefttableview == tableView) //二级分类
    {
        
        if( [[[arraydata objectAtIndex:rightflag] objectForKey:@"s_class"] isKindOfClass:[NSArray class]])
        {
            return [[[arraydata objectAtIndex:rightflag] objectForKey:@"s_class"] count]+1;
        }
        else
        {
            return 1;
        }
    }
    else
    {
        return [arraydata count];
        
    }

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(lefttableview == tableView) //二级分类
    {
        return 35;

    }
    else
    {
        return 70;

    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(lefttableview == tableView)
    {
        ;
    }
    else
    {
        rightflag = indexPath.row;
        [righttableview reloadData];
        [lefttableview reloadData];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(lefttableview == tableView) //二级分类
    {
        static NSString* strtoday = @"secclass";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:strtoday];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strtoday];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UIImage* imagetouch = [UIImage imageNamed:@"touchdownclasslistleft"];
            UIImage* imagenotouch = [UIImage imageNamed:@"notouchclasslistleft"];
            UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(tableView.frame.size.width-imagenotouch.size.width, 0, imagenotouch.size.width, imagenotouch.size.height)];
            button.tag = 777;
            [button setBackgroundImage:imagenotouch forState:UIControlStateNormal];
            [button setBackgroundImage:imagetouch forState:UIControlStateHighlighted];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
            [button addTarget:self action:@selector(secbuttondown:) forControlEvents:UIControlEventTouchUpInside];
            button.titleLabel.font = [UIFont systemFontOfSize:12];
            [cell addSubview:button];
        }
        UIButton* button = (UIButton*)[cell viewWithTag:777];
        button.titleLabel.tag = indexPath.row+100;
        if(indexPath.row==0)
        {
            [button setTitle:@"全部" forState:UIControlStateNormal];
            [button setTitle:@"全部" forState:UIControlStateHighlighted];
        }
        else
        {
            
            [button setTitle:[[[[arraydata objectAtIndex:rightflag] objectForKey:@"s_class"] objectAtIndex:indexPath.row-1] objectForKey:@"title"] forState:UIControlStateNormal];
            [button setTitle:[[[[arraydata objectAtIndex:rightflag] objectForKey:@"s_class"] objectAtIndex:indexPath.row-1] objectForKey:@"title"] forState:UIControlStateHighlighted];
        }
        return cell;
    }
    else
    {
        static NSString* stringmainviewtoday = @"fistclass";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:stringmainviewtoday];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:stringmainviewtoday];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            UIImageView* imageview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
            imageview.tag = 999;
            imageview.backgroundColor = [UIColor clearColor];
            [cell addSubview:imageview];
            
            UILabel* lable = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
            lable.tag = 998;
            lable.backgroundColor = [UIColor clearColor];
            lable.textAlignment = NSTextAlignmentCenter;
            lable.numberOfLines = 0;
            lable.font = [UIFont systemFontOfSize:16];
            [cell addSubview:lable];
            
            UIImage* imagemiddle = [UIImage imageNamed:@"middleclasslistright"];
            UIImageView* imagemiddleview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, imagemiddle.size.width, imagemiddle.size.height)];
            imagemiddleview.tag = 997;
            imagemiddleview.image = imagemiddle;
            [cell addSubview:imagemiddleview];
        }
        UIImageView* imageview = (UIImageView*)[cell viewWithTag:999];
        UIImageView* imagemiddleview = (UIImageView*)[cell viewWithTag:997];
        UILabel* label = (UILabel*)[cell viewWithTag:998];

        if(rightflag==indexPath.row)
        {
            UIImage* imageyuan = [UIImage imageNamed:@"touchdownclasslistright"];
            imageview.image = imageyuan;
            label.textColor = [UIColor whiteColor];
            imagemiddleview.hidden = NO;
            
            
        }
        else
        {
            UIImage* imageyuan = [UIImage imageNamed:@"notouchclasslistright"];
            imageview.image = imageyuan;
            label.textColor = [UIColor blackColor];
            imagemiddleview.hidden = YES;
        }
        imageview.frame = CGRectMake(tableView.frame.size.width-imageview.image.size.width-10, 0, imageview.image.size.width, imageview.image.size.height);
        imagemiddleview.frame = CGRectMake(imageview.frame.origin.x-imagemiddleview.frame.size.width, (imageview.frame.size.height-imagemiddleview.frame.size.height)/2, imagemiddleview.frame.size.width, imagemiddleview.frame.size.height);
        label.text = [[arraydata objectAtIndex:indexPath.row] objectForKey:@"title"];
        label.frame = CGRectMake(imageview.frame.origin.x+5, imageview.frame.origin.y+5, imageview.frame.size.width-10, imageview.frame.size.height-10);
        return cell;
    }

  
    
}


-(void)secbuttondown:(UIButton*)button
{
    int num = button.titleLabel.tag-100;
    if(num==0)
    {
        [self.delegate  searchclassindex:[[arraydata objectAtIndex:rightflag] objectForKey:@"id"] title:[[arraydata objectAtIndex:rightflag] objectForKey:@"title"]];
        
    }
    else
    {
        [self.delegate  searchclassindex:[[[[arraydata objectAtIndex:rightflag] objectForKey:@"s_class"] objectAtIndex:num-1] objectForKey:@"id"] title:[[[[arraydata objectAtIndex:rightflag] objectForKey:@"s_class"] objectAtIndex:num-1] objectForKey:@"title"]];
        
    }

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
