//
//  MainClassGroupCellelement.h
//  cherryproduct
//
//  Created by umessage on 14-8-9.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol MainClassGroupCellelementdelegate <NSObject>

-(void)touchdownMainClassGroupCellelementindex:(int)index numrow:(int)row; //点击分类的顺序－1为1级分类二级分类从0开始 index参数为第几个cell

@end

@interface MainClassGroupCellelement : UIView //分类cell上的元素
{
    NSMutableDictionary* dicdata;
}
@property(assign,nonatomic) id<MainClassGroupCellelementdelegate> delegate;
@property(assign,nonatomic) int maintag;//第几个cell
-(void)getusedata:(NSDictionary*)dic; //页面所需数据
@end
