//
//  MainClassGroupCellelement.m
//  cherryproduct
//
//  Created by umessage on 14-8-9.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "MainClassGroupCellelement.h"
#import "UIImageView+AFNetworking.h"

@implementation MainClassGroupCellelement
@synthesize delegate;
@synthesize maintag;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)getusedata:(NSDictionary*)dic
{
    if(dicdata)
    {
        [dicdata removeAllObjects];
    }
    dicdata = [[NSMutableDictionary alloc] initWithDictionary:dic copyItems:YES];
    
    
    for(UIView* view in [self subviews])
    {
        [view removeFromSuperview];
    }
    
    UIImageView* imageview = [[UIImageView alloc] initWithFrame:self.frame];//背景图片
    NSMutableString* stringimageurl = [[NSMutableString alloc] init];
    [stringimageurl appendString:JPG];
    [stringimageurl appendString:[dicdata objectForKey:@"img"]];
    [imageview setImageWithURL:[NSURL URLWithString:stringimageurl] placeholderImage:[UIImage imageNamed:@"todaygrouploading"]];
    [self addSubview:imageview];//添加背景图片
    UIImage* image = [UIImage imageNamed:@"classmainviefu"];
    UIImage* imagesec = [UIImage imageNamed:@"classmainviewxihusec"];
    int x = 10;
    int num = 1;
    if([[dicdata objectForKey:@"s_class"] isKindOfClass:[NSArray class]])
    {
        num = [[dicdata objectForKey:@"s_class"] count]+1;
    }
    for(int i = 0 ; i< num ; i++) //添加分类button
    {
        UIButton* button = [[UIButton alloc] initWithFrame:CGRectMake(10,10,10,10)];
        if(i==0)
        {
            button.frame = CGRectMake(x, self.frame.size.height-image.size.height-10, image.size.width, image.size.height);
            [button setTitle:[dicdata objectForKey:@"title"] forState:UIControlStateNormal];
            button.titleLabel.numberOfLines = 0;
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            button.titleLabel.font = [UIFont systemFontOfSize:22];
            button.titleLabel.textAlignment = NSTextAlignmentCenter;
            [button setBackgroundImage:image forState:UIControlStateNormal];
            [button setTitleEdgeInsets:UIEdgeInsetsMake(0,7,0,7)];
            button.tag = -1;
            [button addTarget:self action:@selector(toucdownbutton:) forControlEvents:UIControlEventTouchDown];

        }
        else
        {
            button.frame = CGRectMake(x, self.frame.size.height-imagesec.size.height-10, imagesec.size.width, imagesec.size.height);
            [button setTitle:[[[dicdata objectForKey:@"s_class"] objectAtIndex:i-1] objectForKey:@"title"] forState:UIControlStateNormal];
            button.titleLabel.numberOfLines = 0;
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            button.titleLabel.font = [UIFont systemFontOfSize:11];
            button.titleLabel.textAlignment = NSTextAlignmentCenter;
            [button setTitleEdgeInsets:UIEdgeInsetsMake(0,7,0,7)];
            [button setBackgroundImage:imagesec forState:UIControlStateNormal];
            button.tag = 99+i;
            [button addTarget:self action:@selector(toucdownbutton:) forControlEvents:UIControlEventTouchDown];
        }
        [self addSubview:button];
        x = x+button.frame.size.width+5;
    }

}


-(void)toucdownbutton:(UIButton*)button
{
    int row = button.tag;
    if(row>0)
    {
        row = button.tag-100;
    }
    [self.delegate touchdownMainClassGroupCellelementindex:self.maintag-100 numrow:row];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
