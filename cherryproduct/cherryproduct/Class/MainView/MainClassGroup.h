//
//  MainClassGroup.h
//  cherryproduct
//
//  Created by umessage on 14-8-9.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainClassGroupCellelement.h"
@protocol MainClassGroupDelegate <NSObject>

-(void)MainClassGroupDownIndex:(int)indexdown numrow:(int)row; //点击的某一行里面某一个分类从-1开始 -1为一级分类 二级分类从0开始

@end

@interface MainClassGroup : UIView<UITableViewDataSource,UITableViewDelegate,MainClassGroupCellelementdelegate>
{
    UITableView* tableview;
    NSMutableDictionary* dicdata;
    
}

@property(assign,nonatomic) id<MainClassGroupDelegate> delegate;

-(void)getusedata:(NSDictionary*)dic; //页面所需数据
@end
