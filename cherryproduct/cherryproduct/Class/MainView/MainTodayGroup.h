//
//  MainTodayGroup.h
//  cherryproduct
//
//  Created by umessage on 14-8-6.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MainTodayGroupDelegate <NSObject>

-(void)MainTodayGroupDownIndex:(int)indexdown; //点击的某一个今日团购从0开始

@end
//今日团购
@interface MainTodayGroup : UIView <UITableViewDataSource,UITableViewDelegate>
{
    BOOL isleftright; //是右边显示还是左边显示
    UITableView* tableview;
    NSTimer* timer;
    NSMutableDictionary* dicdata;

}

@property(assign,nonatomic) id<MainTodayGroupDelegate> delegate;

-(void)getusedata:(NSDictionary*)dic; //页面所需数据

@end
