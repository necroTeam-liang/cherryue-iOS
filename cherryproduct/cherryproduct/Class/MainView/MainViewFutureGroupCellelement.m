//
//  MainViewFutureGroupCellelement.m
//  cherryproduct
//
//  Created by umessage on 14-8-10.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "MainViewFutureGroupCellelement.h"
#import "UIImageView+AFNetworking.h"
@implementation MainViewFutureGroupCellelement
@synthesize delegate;
@synthesize mytag;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        UIImageView*imageviewback = [[UIImageView alloc] initWithFrame:self.frame];
        imageviewback.tag = 99;
        [self addSubview:imageviewback]; //背景图片
        
        UIImage* imageupback = [UIImage imageNamed:@"mainviewfutureback"];
        UIImageView*imageviewupback = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,imageupback.size.width, imageupback.size.height)];
        imageviewupback.image = imageupback;
        imageviewupback.tag = 100;
        [self addSubview:imageviewupback]; //蒙板图片
        
        UILabel* labeltitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 25, imageviewupback.frame.size.width, 20)];
        labeltitle.tag = 101;
        labeltitle.font = [UIFont systemFontOfSize:20];
        labeltitle.text = @"游泳装备";
        labeltitle.textAlignment = NSTextAlignmentCenter;
        labeltitle.textColor = [UIColor blackColor];
        [self addSubview:labeltitle]; //文字分类
        
        UILabel* labeltime = [[UILabel alloc] initWithFrame:CGRectMake(0, labeltitle.frame.origin.y+labeltitle.frame.size.height+15, imageviewupback.frame.size.width, 20)];
        labeltime.tag = 102;
        labeltime.font = [UIFont systemFontOfSize:20];
        labeltime.text = @"21321312321";
        labeltime.textAlignment = NSTextAlignmentCenter;
        labeltime.textColor = [UIColor blackColor];
        [self addSubview:labeltime]; //开始时间
        
        UIImage* buttonimage = [UIImage imageNamed:@"mainviewfuturebuttup"];
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake((imageupback.size.width-buttonimage.size.width)/2, labeltime.frame.origin.y+labeltime.frame.size.height+15,buttonimage.size.width, buttonimage.size.height)];
        [button setTitle:@"上线提醒" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [button setBackgroundImage:buttonimage forState:UIControlStateNormal];
        [button addTarget:self action:@selector(touchdownbutton:) forControlEvents:UIControlEventTouchDown];
        button.tag = 103;
        [self addSubview:button]; //上线提醒按钮
    }
    return self;
}


-(void)touchdownbutton:(UIButton*)button
{
    [self.delegate MainViewFutureGroupCellelementDownIndex:self.mytag-100];
}

-(void)getusedata:(NSDictionary*)dic  leftright:(int)flag
{
    if(dicdata)
    {
        [dicdata removeAllObjects];
    }
    dicdata = [[NSMutableDictionary alloc] initWithDictionary:dic copyItems:YES];

    
    UIImageView* backimage = (UIImageView*)[self viewWithTag:99]; //背景图片
    NSMutableString* stringurlimage = [[NSMutableString alloc] init];
    [stringurlimage appendString:JPG];
    [stringurlimage appendString:[dicdata objectForKey:@"photo"]];
    [backimage setImageWithURL:[NSURL URLWithString:stringurlimage] placeholderImage:[UIImage imageNamed:@"todaygrouploading"]];

    
    
    UIImageView* backupimage = (UIImageView*)[self viewWithTag:100]; //蒙板图片
    
    UILabel* labeltiele = (UILabel*)[self viewWithTag:101]; //抬头分类
    labeltiele.text = [dicdata objectForKey:@"title"];
    
    UILabel* labeltime = (UILabel*)[self viewWithTag:102]; //上线时间
    NSDate* datenow = [NSDate date];
    NSDateFormatter* dateformat = [[NSDateFormatter alloc] init];
    [dateformat setDateFormat:@"HH:mm:ss"];
    double timetemp = [[dicdata objectForKey:@"start_time"] doubleValue] - [datenow timeIntervalSince1970];
    if(timetemp>0)
    {
        labeltime.text = [dateformat stringFromDate:[NSDate dateWithTimeIntervalSince1970:timetemp]];

    }
    else
    {
        labeltime.text = @"已开始";

    }

    UIButton* button = (UIButton*)[self viewWithTag:103]; //上线提示按钮
    NSString* flagapply = [dicdata objectForKey:@"is_apply"]; //0为未设置
    if([flagapply isEqualToString:@"0"]) //未设置上线提醒
    {
        UIImage* image = [UIImage imageNamed:@"mainviewfuturebuttup"];
        [button setBackgroundImage:image forState:UIControlStateNormal];
        [button setTitle:@"上线提醒" forState:UIControlStateNormal];
    }
    else
    {
        UIImage* image = [UIImage imageNamed:@"mainviewfuturebuttondown"];
        [button setBackgroundImage:image forState:UIControlStateNormal];
        [button setTitle:@"提醒已设" forState:UIControlStateNormal];
    }
    
    if(flag==0) //右边显示
    {
        backupimage.frame = CGRectMake(self.frame.size.width-backupimage.image.size.width, backupimage.frame.origin.y, backupimage.frame.size.width, backimage.frame.size.height);
        labeltiele.frame = CGRectMake(backupimage.frame.origin.x, labeltiele.frame.origin.y, labeltiele.frame.size.width, labeltiele.frame.size.height);
        
        labeltime.frame = CGRectMake(labeltiele.frame.origin.x, labeltime.frame.origin.y, labeltime.frame.size.width, labeltime.frame.size.height);
        
        button.frame = CGRectMake((backupimage.frame.size.width-button.frame.size.width)/2+backupimage.frame.origin.x, button.frame.origin.y, button.frame.size.width,button.frame.size.height);
        
    }
    else
    {
        backupimage.frame = CGRectMake(0,0, backupimage.frame.size.width, backimage.frame.size.height);
        labeltiele.frame = CGRectMake(backupimage.frame.origin.x, labeltiele.frame.origin.y, labeltiele.frame.size.width, labeltiele.frame.size.height);
        
        labeltime.frame = CGRectMake(labeltiele.frame.origin.x, labeltime.frame.origin.y, labeltime.frame.size.width, labeltime.frame.size.height);
        
        button.frame = CGRectMake((backupimage.frame.size.width-button.frame.size.width)/2+backupimage.frame.origin.x, button.frame.origin.y, button.frame.size.width,button.frame.size.height);

    }
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
