//
//  MainViewFutureGroup.h
//  cherryproduct
//
//  Created by umessage on 14-8-10.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewFutureGroupCellelement.h"

@protocol MainViewFutureGroupDelegate <NSObject>

-(void)MainViewFutureGroupDownIndex:(int)indexdown numrow:(int)row; //点击的某一个为开始的团购或者设置开团提醒0为点击的为开团的团购1为上线提醒 , row为点击的第几个

@end
//今日未开团购
@interface MainViewFutureGroup : UIView<UITableViewDataSource,UITableViewDelegate,MainViewFutureGroupCellelementDelegate>
{
    BOOL isleftright; //左边显示还是右边显示 no为右边
    UITableView* tableview;
    NSTimer* timer;
    NSMutableDictionary* dicdata;
}

@property(assign,nonatomic) id<MainViewFutureGroupDelegate> delegate;

-(void)getusedata:(NSDictionary*)dic; //页面所需数据
@end
