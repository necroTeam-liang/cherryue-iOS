//
//  MainViewFutureGroupCellelement.h
//  cherryproduct
//
//  Created by umessage on 14-8-10.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MainViewFutureGroupCellelementDelegate <NSObject>

-(void)MainViewFutureGroupCellelementDownIndex:(int)indexdown;

@end
@interface MainViewFutureGroupCellelement : UIView
{
    NSMutableDictionary* dicdata;

}
@property(assign,nonatomic) id<MainViewFutureGroupCellelementDelegate> delegate;
@property(assign,nonatomic) int mytag;

-(void)getusedata:(NSDictionary*)dic leftright:(int)flag; //页面所需数据 左边显示还是右边显示0为右1为左

@end
