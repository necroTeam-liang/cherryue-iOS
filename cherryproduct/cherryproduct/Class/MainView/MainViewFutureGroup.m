//
//  MainViewFutureGroup.m
//  cherryproduct
//
//  Created by umessage on 14-8-10.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "MainViewFutureGroup.h"
@implementation MainViewFutureGroup
@synthesize delegate;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        isleftright = NO;
    }
    return self;
}

-(void)starttimer //倒计时
{
    BOOL isstoptimer = YES;

    for(int i = 0 ; i < [[dicdata objectForKey:@"data"] count] ; i++)
    {
        NSIndexPath* indexpath = [NSIndexPath indexPathForRow:i inSection:0];
        UITableViewCell* cell = [tableview cellForRowAtIndexPath:indexpath];
        MainViewFutureGroupCellelement* cellelement = (MainViewFutureGroupCellelement*)[cell viewWithTag:1002];
        
        UILabel*labeltime = (UILabel*)[cellelement viewWithTag:102]; //团购时间
        NSDateFormatter * formaater = [[NSDateFormatter alloc] init];
        [formaater setDateFormat:@"HH:mm:ss"];
        NSDate* date = [NSDate date];
        double timetemp = [[[[dicdata objectForKey:@"data"] objectAtIndex:i] objectForKey:@"start_time"] doubleValue] - [date timeIntervalSince1970];
        if(timetemp>0)
        {
            NSString* string =[formaater stringFromDate:[NSDate dateWithTimeIntervalSince1970:timetemp]];
            labeltime.text = string;
            isstoptimer = NO;

        }
        else
        {
            labeltime.text = @"已开始";
        }
    }
    if(isstoptimer)
    {
        if([timer isValid])
        {
            [timer invalidate];
        }
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[dicdata objectForKey:@"data"] count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [UIImage imageNamed:@"mainviewfutureback"].size.height;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.delegate MainViewFutureGroupDownIndex:0 numrow:indexPath.row];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* stringmainviewtoday = @"mainviewfuture";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:stringmainviewtoday];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:stringmainviewtoday];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        MainViewFutureGroupCellelement* cellelement = [[MainViewFutureGroupCellelement alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, [UIImage imageNamed:@"mainviewfutureback"].size.height)];
        cellelement.delegate = self;
        cellelement.tag = 1002;
        [cell addSubview:cellelement];//cell的元素
        
    }
    MainViewFutureGroupCellelement* element = (MainViewFutureGroupCellelement*)[cell viewWithTag:1002];
    element.mytag = indexPath.row+100;
    int flag = indexPath.row%2;
    if(flag==0)
    {
        isleftright = NO;
    }
    else
    {
        isleftright = YES;
    }
    [element getusedata:[[dicdata objectForKey:@"data"] objectAtIndex:indexPath.row] leftright:isleftright];
    return cell;
    
}
-(void)dealloc
{
    if([timer isValid])
    {
        [timer invalidate];
        
    }
}

-(void)getusedata:(NSDictionary*)dic
{
    if(dicdata)
    {
        [dicdata removeAllObjects];
    }
    dicdata = [[NSMutableDictionary alloc] initWithDictionary:dic copyItems:YES];
    
    tableview = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height) style:UITableViewStylePlain];
    tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableview.delegate = self;
    tableview.dataSource = self;
    [self addSubview:tableview];
    timer =  [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(starttimer) userInfo:nil repeats:YES];
}


-(void)MainViewFutureGroupCellelementDownIndex:(int)indexdown //点击上线提醒
{
    [self.delegate MainViewFutureGroupDownIndex:1 numrow:indexdown];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
