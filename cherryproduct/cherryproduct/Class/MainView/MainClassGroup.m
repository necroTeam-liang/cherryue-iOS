//
//  MainClassGroup.m
//  cherryproduct
//
//  Created by umessage on 14-8-9.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "MainClassGroup.h"
#define TABLEVIEWCELLHEIGHT 145
@implementation MainClassGroup
@synthesize delegate;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[dicdata objectForKey:@"data"] count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TABLEVIEWCELLHEIGHT;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* stringmainviewtoday = @"mainclassview";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:stringmainviewtoday];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:stringmainviewtoday];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        MainClassGroupCellelement* element = [[MainClassGroupCellelement alloc] initWithFrame:CGRectMake(0,0, DEVICE_BOUNDS_WIDTH, TABLEVIEWCELLHEIGHT)];
        element.delegate =self;
        element.tag = 1001;
        [cell addSubview:element];//添加cell
        
    }
    MainClassGroupCellelement* element = (MainClassGroupCellelement*)[cell viewWithTag:1001];
    element.maintag = indexPath.row+100;
    [element getusedata:[[dicdata objectForKey:@"data"] objectAtIndex:indexPath.row]];
    return cell;
    
}


-(void)getusedata:(NSDictionary*)dic
{
    if(dicdata)
    {
        [dicdata removeAllObjects];
    }
    dicdata = [[NSMutableDictionary alloc] initWithDictionary:dic copyItems:YES];
    
    tableview = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height) style:UITableViewStylePlain];
    tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableview.delegate = self;
    tableview.dataSource = self;
    [self addSubview:tableview];
}


-(void)touchdownMainClassGroupCellelementindex:(int)index  numrow:(int)row//点击的cell里面的顺序
{
    [self.delegate MainClassGroupDownIndex:index numrow:row];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
