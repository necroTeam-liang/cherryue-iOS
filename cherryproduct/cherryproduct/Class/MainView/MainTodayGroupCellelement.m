//
//  MainTodayGroupCellelement.m
//  cherryproduct
//
//  Created by umessage on 14-8-6.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "MainTodayGroupCellelement.h"

@implementation MainTodayGroupCellelement

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
//        UIImageView*imageviewbackview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
//        imageviewbackview.tag = 101;
//        [imageviewbackview setImage:[UIImage imageNamed:@"mainviewtodayred"]];
//        [imageviewbackview setFrame:CGRectMake(0, 0, [UIImage imageNamed:@"mainviewtodayred"].size.width, [UIImage imageNamed:@"mainviewtodayred"].size.height)];
//        self.frame = imageviewbackview.frame;
//        self.backgroundColor = [UIColor clearColor];
//        [self addSubview:imageviewbackview]; //蒙板颜色
        
        UIImageView*imageviewbackview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        imageviewbackview.tag = 101;
        imageviewbackview.alpha = 0.7;
        imageviewbackview.layer.cornerRadius = 70;
        imageviewbackview.layer.masksToBounds = YES;
        [imageviewbackview setFrame:CGRectMake(0, 0, [UIImage imageNamed:@"mainviewtodayred"].size.width, [UIImage imageNamed:@"mainviewtodayred"].size.height)];
        self.frame = imageviewbackview.frame;
        [self addSubview:imageviewbackview]; //蒙板颜色
        
        UILabel* labletitle = [[UILabel alloc] initWithFrame:CGRectMake(0,28, self.frame.size.width, 18)];
        labletitle.text = @"睡眠装备";
        labletitle.font = [UIFont systemFontOfSize:18];
        labletitle.textAlignment = NSTextAlignmentCenter;
        labletitle.backgroundColor = [UIColor clearColor];
        labletitle.textColor = [UIColor whiteColor];
        labletitle.tag = 102;
        [self addSubview:labletitle]; //团购名称
        
        UILabel* labelmon = [[UILabel alloc] init];
        labelmon.backgroundColor = [UIColor clearColor];
        labelmon.font = [UIFont boldSystemFontOfSize:28];
        labelmon.text = @"25";
        CGSize sizeToFit = [labelmon.text sizeWithFont:[UIFont boldSystemFontOfSize:28] constrainedToSize:CGSizeMake(999, 999) lineBreakMode:UILineBreakModeWordWrap];
        labelmon.frame = CGRectMake((self.frame.size.width-sizeToFit.width)/2, (self.frame.size.height-sizeToFit.height)/2, sizeToFit.width, sizeToFit.height);
        labelmon.textColor = [UIColor whiteColor];
        labelmon.tag = 103;
        [self addSubview:labelmon];//团购价格
        
        UILabel* labelmonflag = [[UILabel alloc] initWithFrame:CGRectMake(labelmon.frame.origin.x-13, labelmon.frame.origin.y+labelmon.frame.size.height-16-2, 14, 14)];
        labelmonflag.backgroundColor = [UIColor clearColor];
        labelmonflag.font = [UIFont systemFontOfSize:14];
        labelmonflag.text = @"￥";
        labelmonflag.textColor = [UIColor whiteColor];
        labelmonflag.textAlignment = NSTextAlignmentCenter;
        labelmonflag.tag = 104;
        [self addSubview:labelmonflag];//团购价格符号
        
        UILabel* labelname = [[UILabel alloc] initWithFrame:CGRectMake(labelmon.frame.origin.x+labelmon.frame.size.width, labelmon.frame.origin.y+labelmon.frame.size.height-12-5, 12, 12)];
        labelname.backgroundColor = [UIColor clearColor];
        labelname.font = [UIFont systemFontOfSize:12];
        labelname.text = @"起";
        labelname.textColor = [UIColor whiteColor];
        labelname.textAlignment = NSTextAlignmentCenter;
        labelname.tag = 105;
        [self addSubview:labelname];//团购价格起
        
        NSDateFormatter * formaater = [[NSDateFormatter alloc] init];
        [formaater setDateFormat:@"HH:mm:ss"];
        UILabel* labeltime = [[UILabel alloc] initWithFrame:CGRectMake(0, 100, self.frame.size.width, 18)];
        labeltime.backgroundColor = [UIColor clearColor];
        labeltime.font = [UIFont systemFontOfSize:18];
        labeltime.text = [formaater stringFromDate:[NSDate date]];
        labeltime.textColor = [UIColor blackColor];
        labeltime.textAlignment = NSTextAlignmentCenter;
        labeltime.tag = 106;
        [self addSubview:labeltime];//团购时间

        
    }
    return self;
}

-(void)getusedata:(NSDictionary*)dic leftright:(int)flag
{
    if(dicdata)
    {
        [dicdata removeAllObjects];
    }
    dicdata = [[NSMutableDictionary alloc] initWithDictionary:dic copyItems:YES];
    
    
    UIImageView* imageback = (UIImageView*)[self viewWithTag:101]; //蒙板颜色
    imageback.backgroundColor = [self colorWithHexString:[dicdata objectForKey:@"color"]];

    
    UILabel*title = (UILabel*)[self viewWithTag:102]; //团购名称
    title.text = [dicdata objectForKey:@"title"];
    
    
    
    UILabel*labelmon = (UILabel*)[self viewWithTag:103]; //团购价格
    labelmon.text = [dicdata objectForKey:@"price"];
    CGSize sizeToFit = [labelmon.text sizeWithFont:[UIFont boldSystemFontOfSize:28] constrainedToSize:CGSizeMake(999, 999) lineBreakMode:UILineBreakModeWordWrap];
    labelmon.frame = CGRectMake((self.frame.size.width-sizeToFit.width)/2, (self.frame.size.height-sizeToFit.height)/2, sizeToFit.width, sizeToFit.height);
    
    
    UILabel*labelmonflag = (UILabel*)[self viewWithTag:104]; //团购价格符号
    labelmonflag.frame = CGRectMake(labelmon.frame.origin.x-13, labelmon.frame.origin.y+labelmon.frame.size.height-16-2, 14, 14);
    
    
    UILabel*labelname = (UILabel*)[self viewWithTag:105]; //团购价格起
    labelname.frame = CGRectMake(labelmon.frame.origin.x+labelmon.frame.size.width, labelmon.frame.origin.y+labelmon.frame.size.height-12-5, 12, 12);
    
    

    UILabel*labeltime = (UILabel*)[self viewWithTag:106]; //团购时间
    NSDateFormatter * formaater = [[NSDateFormatter alloc] init];
    [formaater setDateFormat:@"HH:mm:ss"];
    long long timeover = [[dicdata objectForKey:@"end_time"] longLongValue] - [[NSDate date] timeIntervalSince1970];
    if(timeover>0)
    {
        labeltime.text = [formaater stringFromDate:[[NSDate alloc] initWithTimeIntervalSince1970:timeover]];
    }
    else
    {
        labeltime.text = @"已过期";

    }
    
    
    if(flag==0) //左边显示
    {
        
        self.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    }
    else
    {
        self.frame = CGRectMake(DEVICE_BOUNDS_WIDTH-self.frame.size.width, 0, self.frame.size.width, self.frame.size.height);

    }
    
    
}


//设置字体颜色
- (UIColor *) colorWithHexString: (NSString *) stringToConvert
{
    NSString *cString = [[stringToConvert stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    
    if ([cString length] < 6)
        return [UIColor whiteColor];
    if ([cString hasPrefix:@"#"])
        cString = [cString substringFromIndex:1];
    if ([cString length] != 6)
        return [UIColor whiteColor];
    
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
