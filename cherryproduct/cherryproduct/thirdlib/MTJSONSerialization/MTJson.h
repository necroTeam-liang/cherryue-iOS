//
//  MTJson.h
//  MTravel
//
//  Created by zhao on 14-2-21.
//  Copyright (c) 2014年 umtravelteam. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MTJson : NSObject
+ (id)jsonWithString:(NSString*)jsonStr;
@end
@interface NSString (MTJSONSerialization)
- (id)JSONValue;
@end
@interface NSDictionary (MTJSONSerialization)
- (NSString *)JSONString;
@end