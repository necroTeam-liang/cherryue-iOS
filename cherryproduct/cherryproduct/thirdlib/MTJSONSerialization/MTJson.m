//
//  MTJson.m
//  MTravel
//
//  Created by zhao on 14-2-21.
//  Copyright (c) 2014年 umtravelteam. All rights reserved.
//

#import "MTJson.h"

@implementation MTJson
+ (id)jsonWithString:(NSString*)jsonStr{
    NSError *error;
    NSData* jsonData = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
    NSLog(@"json = %@",json);
    if (json == nil) {
        NSLog(@"json parse failed \r\n");
        return nil;
    }
    return json;
}
@end
@implementation NSString (MTJSONSerialization)
- (id)JSONValue
{
    return [MTJson jsonWithString:self];
}
@end
@implementation NSDictionary (MTJSONSerialization)
- (NSString *)JSONString
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
}
@end

@implementation NSData (MTJSONSerialization)
- (NSString *)JSONString
{
//    NSError *error;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:self encoding:NSUTF8StringEncoding];
    return jsonString;
}
@end