//
//  Header.h
//  cherryproduct
//
//  Created by umessage on 14-8-4.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#ifndef cherryproduct_Header_h
#define cherryproduct_Header_h

#define UmengAppkey @"5211818556240bc9ee01db2f"

#define kAppKey         @"4198576750"
#define kRedirectURI    @"http://www.sina.com"

#define JPG @"http://42.120.48.241/Attachment/"


#define SERVER_IP @"http://42.120.48.241/api.php"


#define NLSystemVersionGreaterOrEqualThan(version) ([[[UIDevice currentDevice] systemVersion] floatValue] >= version)
#define IOS7_OR_LATER NLSystemVersionGreaterOrEqualThan(7.0)
//屏幕宽度
#define SCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)
//屏幕高度
#define SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)
//ios系统版本
#define IOS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]
//读取本地图片 两个参数，前面一个是 文件名，后面一个是类型 （没有缓存）
#define LOADIMAGE(file,ext) [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:file ofType:ext]]
// rgb颜色转换（16进制->10进制）
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define colorWithRGBA(r,g,b,a) [UIColor colorWithRed:1.0/255*(r) green:1.0/255*(g) blue:1.0/255*(b) alpha:(a)]

#define IS_IOS7 ([[UIDevice  currentDevice].systemVersion floatValue]>=7.0)?1:0
#define DEVICE_BOUNDS_HEIGHT [UIScreen mainScreen].bounds.size.height
#define DEVICE_BOUNDS_WIDTH [UIScreen mainScreen].bounds.size.width
#define DEVICE_CURRENT_HEIGHT [UIScreen mainScreen].currentMode.size.height
#define DEVICE_CURRENT_WIDTH [UIScreen mainScreen].currentMode.size.width






#endif
