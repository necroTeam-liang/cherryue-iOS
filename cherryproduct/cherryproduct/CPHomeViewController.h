//
//  ViewController.h
//  cherryproduct
//
//  Created by umessage on 14-8-4.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainTodayGroup.h"
#import "MainClassGroup.h"
#import "MainViewFutureGroup.h"
@interface CPHomeViewController :BaseViewController<MainTodayGroupDelegate,MainClassGroupDelegate,MainViewFutureGroupDelegate>
{
    BOOL isfuturnpage; //是不是未开团页面
    NSMutableDictionary* datajson;//导航条上面的某一项团购的数据（今日分类预告）
}
@end
