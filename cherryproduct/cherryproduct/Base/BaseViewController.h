//
//  BaseViewController.h
//  MTravel
//
//  Created by umios on 14-1-22.
//  Copyright (c) 2014年 umtravelteam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
typedef enum
{
    RIGHT_SHARE = 80011,      //分享
    RIGHT_CENTRE = 80012,       //个人中心
    
    RIGHT_MORE = 80013,       //更多
    RIGHT_RING = 80014,       //提醒
    RIGHT_MESSAGE = 80015,       //留言
    RIGHT_SEARCH =80016,        //搜索
    RIGHT_SETTING = 80017,       //设置
    RIGHT_SHARE_MESSAGE = 81871,       //分享+留言
    RIGHT_SEARCH_CENTRE = 80028,        //搜索+个人中心
    RIGHT_FINISH = 80018,        //完成
    RIGHT_SUBMENT = 80019,  //写评论
    RIGHT_DOWNRING = 10101
}NAVIGATIONITEM_TYPE;
@interface BaseViewController : UIViewController<MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;

}
@property(nonatomic,retain)UIImageView* navigationBar;
@property(nonatomic,retain)UILabel* titleLabel;
@property(nonatomic,assign)BOOL isHomePage;
@property(nonatomic,retain)UIView* myView;


/*************************************************
 Function:  backButtonAction
 Description:   如果页面需要自定义返回按钮可以 重写此方法
 *************************************************/
- (void)backButtonAction:(UIButton*) sender;

/*************************************************
 Function:  setTitle
 Description:   每个页面设置title调用此方法
 title:
 *************************************************/
- (void)setTitle:(NSString *)title;

/*************************************************
 Function:  addNavBarButton
 Description:添加nav左边按钮
 type:NAVIGATIONITEM_TYPE此为三项 
 如果home，phone两个按钮同时存在则：
 [self addNavBarButton:RIGHT_HOME|RIGHT_PHONE];
 或者
 [self addNavBarButton:RIGHT_ITEM];
 两者皆可
 *************************************************/
- (void)addNavBarButton:(NAVIGATIONITEM_TYPE) type;

/*************************************************
 Function:  hidesBackButton
 Description:   隐藏返回键
 isHidden: yes(隐藏)
 *************************************************/
- (void)hidesBackButton:(BOOL) isHidden;

/*************************************************
 Function:  popToViewController
 Description:   直接返回到指定页面
 页面栈里没有这个页面 则执行返回上一页面
 controllerName（NSString） 返回到哪个类的名字
 *************************************************/
- (void)popToViewController:(NSString*)controllerName;
/*************************************************
 Function:  umengShare
 Description:   友盟分享接口
 shareText（NSString） 分享的文字
 分享包括微博，腾讯微博，微信，朋友圈，qq，qq空间，人人，豆瓣，邮箱，短信
 *************************************************/
- (void)umengShare:(NSString*) shareText icon:(UIImage*)imageIocn;
/*************************************************
 Function:  pushToViewController
 Description:   直接跳转到制定页面（包括不同工程 如：酒店，机票等工程）
 通过类名 映射类实例 push到指定界面
 controllerName（NSString） push到哪个类的名字
 animated（bool）是否有动画
 *************************************************/
- (void)pushToViewController:(NSString*)controllerName animated:(BOOL)animated;
/*************************************************
 Function:  createInstance
 Description:   通过类名初始化一个对象（包括不同工程 如：酒店，机票等工程）
 通过类名 映射类实例
 controllerName（NSString） 实例的类名
 *************************************************/
- (id)createInstance:(NSString*)controllerName;
/*************************************************
 Function:  isPagefrom
 Description:   判断来源页面
 controllerName（NSString） 的类名
 *************************************************/
- (BOOL)isPagefrom:(NSString*)controllerName;
- (void)showAlertView:(NSString*)title subtitle:(NSString*)subtitle;
- (void)setNavigationBarColorStr:(NSString*)color;

- (void)setNavigationBarColor:(UIColor*)color;

- (void)shareCallBackUrl:(NSString*)url;

- (void)messageButtonAction:(UIButton*) sender;
- (void)centreButtonAction:(UIButton*) sender;
- (void)moreButtonAction:(UIButton*) sender;
- (void)ringButtonAction:(UIButton*) sender;
- (void)settingButtonAction:(UIButton*) sender;
- (void)shareButtonAction:(UIButton*) sender;
- (void)searchButtonAction:(UIButton*) sender;
- (void)finishButtonAction:(UIButton*) sender;
@end
