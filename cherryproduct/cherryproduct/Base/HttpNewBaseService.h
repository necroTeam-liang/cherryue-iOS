//
//  HttpNewBaseService.h
//  huitravel
//  service 类一定要继承
//  Created by umios on 12-11-12.
//  Copyright (c) 2012年 umios. All rights reserved.
//
#import "AFHTTPSessionManager.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "AFHTTPRequestOperationManager.h"
typedef void (^CommonfileFormDataBlock)(id <AFMultipartFormData> formData);
typedef void (^CommonStringResponseBlock)(NSString *responseString);
typedef void (^CommonErrorBlock)(NSError* error);
@interface HttpNewBaseService : AFHTTPRequestOperationManager <AFNetworkActivityIndicatorManagerDelegate>{
    
}
@property(nonatomic,retain) UIView *loadingView;
@property(nonatomic,assign) BOOL isHidenLoadingView;
+ (instancetype)sharedClient;
/*!
 *  @abstract base method for send an POST request wether ues animation
 *
 *  @discussion
 *	Creates an request
 *  The animated is optional
 *  post
 */
-(void) postRequestOperation:(NSString *) operationPath
             paramDictionary:(NSMutableDictionary *)paramDic
                onCompletion:(CommonStringResponseBlock) completion
                     onError:(CommonErrorBlock) errorBlock
                  onAnimated:(BOOL)animated;
/*!
 *  @abstract base method for send an GET request wether ues animation
 *
 *  @discussion
 *	Creates an request
 *  The animated is optional
 *  get
 */
-(void) getRequestOperation:(NSString *) operationPath
            paramDictionary:(NSMutableDictionary *)paramDic
               onCompletion:(CommonStringResponseBlock) completion
                    onError:(CommonErrorBlock) errorBlock
                 onAnimated:(BOOL)animated;

/*!
 *  @abstract base method for send an POST request wether ues animation
 *
 *  @discussion
 *	Creates an request
 *  The animated is optional
 *  post
 */
-(void) postFileRequestOperation:(NSString *) operationPath
                 paramDictionary:(NSMutableDictionary *)paramDic
       constructingBodyWithBlock:(CommonfileFormDataBlock)fileFormDataBlock
                    onCompletion:(CommonStringResponseBlock) completion
                         onError:(CommonErrorBlock) errorBlock
                      onAnimated:(BOOL)animated;
/*!
 *  @abstract base method for send an GET request wether ues animation
 *
 *  @discussion
 *	Creates an request
 *  The animated is optional
 *  取消网络请求
 */
- (void)cancelRequest;

/*************************************************
 Description:POST上传图片方法
 **************************************************/
-(void) postRequestOperation:(NSString *) operationPath
             paramDictionary:(NSMutableDictionary *)paramDic
                onCompletion:(CommonStringResponseBlock) completion
                     onError:(CommonErrorBlock) errorBlock
                  onAnimated:(BOOL)animated imagedata:(NSData*)dataimage imagekey:(NSString*)stringimagekeyname;

@end
