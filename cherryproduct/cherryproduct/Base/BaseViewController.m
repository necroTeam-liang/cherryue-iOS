//
//  BaseViewController.m
//  MTravel
//
//  Created by umios on 14-1-22.
//  Copyright (c) 2014年 umtravelteam. All rights reserved.
//

#import "BaseViewController.h"
#import "UMSocialSnsService.h"
#import "UMSocialSnsPlatformManager.h"
#import "UMSocial.h"


#import "UMSocialWechatHandler.h"
#import "UMSocialQQHandler.h"
#import "UMSocialSinaHandler.h"
#import "UMSocialTencentWeiboHandler.h"

//#import "UMSocialInstagramHandler.h"

@class UMSocialWechatHandler;


@interface BaseViewController ()

@end

@implementation BaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
     
        _isHomePage = NO;

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    
    _navigationBar = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, DEVICE_BOUNDS_WIDTH, IOS7_OR_LATER?64:44)];
    _navigationBar.userInteractionEnabled = YES;
    [self.view addSubview:_navigationBar];
    
    //返回按钮
    [self addBackButton];
    //title
    [self addTitle];
    //topbar
//    UIImage *tobImage = [UIImage imageNamed:@"NvBar.png"];
    
    [_navigationBar setBackgroundColor:[UIColor colorWithRed:249.0/255.0 green:201.0/255.0 blue:19.0/255.0 alpha:1.0]];

    _myView = [[UIView alloc] initWithFrame:CGRectMake(0, IOS7_OR_LATER?64:44, self.view.frame.size.width, self.view.frame.size.height-(IOS7_OR_LATER?64:44))];
    _myView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_myView];
    //兼容ios7
    if ( IOS7_OR_LATER )
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
    }
    
    self.view.backgroundColor = [UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1.0];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}
- (void)setNavigationBarColorStr:(NSString*)color
{
    if (color||![color isEqualToString:@""]) {
        _navigationBar.backgroundColor = [UIColor hexStringToColor:color];
    }
    
}
- (void)setNavigationBarColor:(UIColor*)color
{
    _navigationBar.backgroundColor = color;
    
}
- (void)addTitle
{
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(110, 20, 100, 44)];
    _titleLabel.textColor = [UIColor whiteColor];
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.font = [UIFont systemFontOfSize:20];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [_navigationBar addSubview:_titleLabel];

}
- (void)addBackButton
{

    self.navigationItem.hidesBackButton =YES;
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, IOS7_OR_LATER?20:0, [UIImage imageNamed:@"topBack.png"].size.width-5, 44)];
    if (IOS7_OR_LATER)
    {
        [backButton setImage:[UIImage imageNamed:@"topBack.png"] forState:UIControlStateNormal];
        [backButton setImage:[UIImage imageNamed:@"topBack.png"] forState:UIControlStateHighlighted];
    }
    else
    {
        [backButton setBackgroundImage:[UIImage imageNamed:@"topBack.png"] forState:UIControlStateNormal];
        [backButton setBackgroundImage:[UIImage imageNamed:@"topBack.png"] forState:UIControlStateHighlighted];
    }

//    backButton.imageEdgeInsets = UIEdgeInsetsMake(0, IOS7_OR_LATER?-32:-10, 0.0, 0);
    [backButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    backButton.tag = 1999;
    
    [_navigationBar addSubview:backButton];

}
//设置title
- (void)setTitle:(NSString *)title
{
    _titleLabel.text = title;
}
- (void)hidesBackButton:(BOOL) isHidden
{
    if (isHidden)
    {
        [_navigationBar viewWithTag:1999].hidden = YES;
    }
    else
    {
        [_navigationBar viewWithTag:1999].hidden = NO;
    }
}
- (void)addNavBarButton:(NAVIGATIONITEM_TYPE) type
{
    switch (type) {
        case RIGHT_CENTRE:
        {
            UIButton *homeButton = [[UIButton alloc] initWithFrame:CGRectMake(DEVICE_BOUNDS_WIDTH-44, IOS7_OR_LATER?20:0, [UIImage imageNamed:@"centre.png"].size.width, 44)];
            [homeButton setImage:[UIImage imageNamed:@"centre.png"] forState:UIControlStateNormal];
            [homeButton setImage:[UIImage imageNamed:@"centre.png"] forState:UIControlStateHighlighted];
            [homeButton addTarget:self action:@selector(centreButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [_navigationBar addSubview:homeButton];
            

        }
            break;
        case RIGHT_MESSAGE:
        {
            UIButton *phoneButton = [[UIButton alloc] initWithFrame:CGRectMake(DEVICE_BOUNDS_WIDTH-44, IOS7_OR_LATER?20:0, [UIImage imageNamed:@"reviewproduct"].size.width-0, 44)];
            [phoneButton setImage:[UIImage imageNamed:@"reviewproduct"] forState:UIControlStateNormal];
            [phoneButton setImage:[UIImage imageNamed:@"reviewproduct"] forState:UIControlStateHighlighted];
            [phoneButton addTarget:self action:@selector(messageButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [_navigationBar addSubview:phoneButton];
            
        }
            break;
        case RIGHT_MORE:
        {
            UIButton *phoneButton = [[UIButton alloc] initWithFrame:CGRectMake(DEVICE_BOUNDS_WIDTH-44, IOS7_OR_LATER?20:0, [UIImage imageNamed:@"more.png"].size.width-0, 44)];
            [phoneButton setImage:[UIImage imageNamed:@"more.png"] forState:UIControlStateNormal];
            [phoneButton setImage:[UIImage imageNamed:@"more.png"] forState:UIControlStateHighlighted];
            [phoneButton addTarget:self action:@selector(moreButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [_navigationBar addSubview:phoneButton];
            
        }
            break;
        case RIGHT_RING:
        {
            UIButton *phoneButton = [[UIButton alloc] initWithFrame:CGRectMake(DEVICE_BOUNDS_WIDTH-44, IOS7_OR_LATER?20:0, [UIImage imageNamed:@"upfuturn"].size.width-0, 44)];
            [phoneButton setImage:[UIImage imageNamed:@"upfuturn"] forState:UIControlStateNormal];
            [phoneButton setImage:[UIImage imageNamed:@"upfuturn"] forState:UIControlStateHighlighted];
            [phoneButton addTarget:self action:@selector(ringButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [_navigationBar addSubview:phoneButton];
            
        }
            break;
        case RIGHT_DOWNRING:
        {
            UIButton *phoneButton = [[UIButton alloc] initWithFrame:CGRectMake(DEVICE_BOUNDS_WIDTH-44, IOS7_OR_LATER?20:0, [UIImage imageNamed:@"downupfuturn"].size.width-0, 44)];
            [phoneButton setImage:[UIImage imageNamed:@"downupfuturn"] forState:UIControlStateNormal];
            [phoneButton setImage:[UIImage imageNamed:@"downupfuturn"] forState:UIControlStateHighlighted];
            [phoneButton addTarget:self action:@selector(ringdownButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [_navigationBar addSubview:phoneButton];
        }
            break;
        case RIGHT_SEARCH:
        {
            UIButton *phoneButton = [[UIButton alloc] initWithFrame:CGRectMake(DEVICE_BOUNDS_WIDTH-44, IOS7_OR_LATER?20:0, [UIImage imageNamed:@"searchButton.png"].size.width-0, 44)];
            [phoneButton setImage:[UIImage imageNamed:@"searchButton.png"] forState:UIControlStateNormal];
            [phoneButton setImage:[UIImage imageNamed:@"searchButton.png"] forState:UIControlStateHighlighted];
            [phoneButton addTarget:self action:@selector(searchButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [_navigationBar addSubview:phoneButton];
            
        }
            break;
        case RIGHT_SETTING:
        {
            UIButton *phoneButton = [[UIButton alloc] initWithFrame:CGRectMake(DEVICE_BOUNDS_WIDTH-44, IOS7_OR_LATER?20:0, [UIImage imageNamed:@"setting.png"].size.width-0, 44)];
            [phoneButton setImage:[UIImage imageNamed:@"setting.png"] forState:UIControlStateNormal];
            [phoneButton setImage:[UIImage imageNamed:@"setting.png"] forState:UIControlStateHighlighted];
            [phoneButton addTarget:self action:@selector(settingButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [_navigationBar addSubview:phoneButton];
            
        }
            break;
        case RIGHT_SHARE:
        {
            UIButton *phoneButton = [[UIButton alloc] initWithFrame:CGRectMake(DEVICE_BOUNDS_WIDTH-44, IOS7_OR_LATER?20:0, [UIImage imageNamed:@"share.png"].size.width-0, 44)];
            [phoneButton setImage:[UIImage imageNamed:@"share.png"] forState:UIControlStateNormal];
            [phoneButton setImage:[UIImage imageNamed:@"share.png"] forState:UIControlStateHighlighted];
            [phoneButton addTarget:self action:@selector(shareButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [_navigationBar addSubview:phoneButton];
            
        }
            break;
        case RIGHT_SEARCH_CENTRE:
        {
            UIButton *phoneButton = [[UIButton alloc] initWithFrame:CGRectMake(DEVICE_BOUNDS_WIDTH-44*2, IOS7_OR_LATER?20:0, [UIImage imageNamed:@"searchButton.png"].size.width-0, 44)];
            [phoneButton setImage:[UIImage imageNamed:@"searchButton.png"] forState:UIControlStateNormal];
            [phoneButton setImage:[UIImage imageNamed:@"searchButton.png"] forState:UIControlStateHighlighted];
            [phoneButton addTarget:self action:@selector(searchButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [_navigationBar addSubview:phoneButton];
            
            
            UIButton *homeButton = [[UIButton alloc] initWithFrame:CGRectMake(DEVICE_BOUNDS_WIDTH-44, IOS7_OR_LATER?20:0, [UIImage imageNamed:@"centre.tif"].size.width-0, 44)];
            [homeButton setImage:[UIImage imageNamed:@"centre.tif"] forState:UIControlStateNormal];
            [homeButton setImage:[UIImage imageNamed:@"centre.tif"] forState:UIControlStateHighlighted];
            [homeButton addTarget:self action:@selector(centreButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [_navigationBar addSubview:homeButton];
        }
            break;
        case RIGHT_SHARE_MESSAGE:
        {
            UIButton *phoneButton = [[UIButton alloc] initWithFrame:CGRectMake(DEVICE_BOUNDS_WIDTH-44*2, IOS7_OR_LATER?20:0, [UIImage imageNamed:@"reviewproduct"].size.width-0, 44)];
            [phoneButton setImage:[UIImage imageNamed:@"reviewproduct"] forState:UIControlStateNormal];
            [phoneButton setImage:[UIImage imageNamed:@"reviewproduct"] forState:UIControlStateHighlighted];
            [phoneButton addTarget:self action:@selector(messageButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [_navigationBar addSubview:phoneButton];
            
            
            UIButton *homeButton = [[UIButton alloc] initWithFrame:CGRectMake(DEVICE_BOUNDS_WIDTH-44, IOS7_OR_LATER?20:0, [UIImage imageNamed:@"share.png"].size.width-0, 44)];
            [homeButton setImage:[UIImage imageNamed:@"share.png"] forState:UIControlStateNormal];
            [homeButton setImage:[UIImage imageNamed:@"share.png"] forState:UIControlStateHighlighted];
            [homeButton addTarget:self action:@selector(shareButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [_navigationBar addSubview:homeButton];
            
        }
            break;

        case RIGHT_FINISH:
        {
            UIButton *phoneButton = [[UIButton alloc] initWithFrame:CGRectMake(DEVICE_BOUNDS_WIDTH-44*2, IOS7_OR_LATER?20:0, 88, 44)];
            [phoneButton setTitle:@"完成" forState:UIControlStateNormal];
            [phoneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            phoneButton.backgroundColor = [UIColor clearColor];
            [phoneButton addTarget:self action:@selector(finishButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [_navigationBar addSubview:phoneButton];
        }
            break;
        case RIGHT_SUBMENT:
        {
            UIButton *phoneButton = [[UIButton alloc] initWithFrame:CGRectMake(DEVICE_BOUNDS_WIDTH-44*2, IOS7_OR_LATER?20:0, 88, 44)];
            [phoneButton setTitle:@"发布" forState:UIControlStateNormal];
            [phoneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            phoneButton.backgroundColor = [UIColor clearColor];
            [phoneButton addTarget:self action:@selector(submentbuttondown:) forControlEvents:UIControlEventTouchUpInside];
            [_navigationBar addSubview:phoneButton];
        }
            break;
        default:
            break;
    }
    
}


-(void)submentbuttondown:(UIButton*)button
{
    
}

- (void)popToViewController:(NSString*)controllerName
{
    if (controllerName.length) {
        for (id cotroller in self.navigationController.viewControllers) {
            if (!strcmp(object_getClassName(cotroller), [controllerName UTF8String])) {
                [self.navigationController popToViewController:(UIViewController *)cotroller animated:YES];
                return;
            }
        }
    }

    [self.navigationController popViewControllerAnimated:YES];
}
- (void)backButtonAction:(UIButton*) sender{
    if (!_isHomePage) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    NSLog(@"pushdownback");
    
}
- (void)umengShare:(NSString*) shareText icon:(UIImage*)imageIocn
{
    [UMSocialSnsService presentSnsIconSheetView:self
                                         appKey:UmengAppkey
                                      shareText:shareText
                                     shareImage:imageIocn?imageIocn:[UIImage imageNamed:@"icon"]
                                shareToSnsNames:[NSArray arrayWithObjects:UMShareToSina,UMShareToTencent,UMShareToWechatSession,UMShareToWechatTimeline,UMShareToQQ,UMShareToQzone,UMShareToDouban,UMShareToSms,nil]
                                       delegate:nil];
}
- (void)pushToViewController:(NSString *)controllerName animated:(BOOL)animated
{
    Class class = NSClassFromString(controllerName);
    id object = [[class alloc] init];
    [[self navigationController] pushViewController:(UIViewController*)object  animated:animated];
}


- (id)createInstance:(NSString*)controllerName
{
    Class class = NSClassFromString(controllerName);
    id object = [[class alloc] init];
    return object;
}

- (BOOL)isPagefrom:(NSString *)controllerName
{
    return ([[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2]isKindOfClass:NSClassFromString(controllerName)]);
}
- (void)shareCallBackUrl:(NSString*)url
{
    //设置微信AppId，url地址传nil，将默认使用友盟的网址，需要#import "UMSocialWechatHandler.h"
    [UMSocialWechatHandler setWXAppId:@"wxd9a39c7122aa6516" url:url];
    [UMSocialQQHandler setSupportWebView:YES];
    //设置手机QQ 的AppId，Appkey，和分享URL，需要#import "UMSocialQQHandler.h"
    [UMSocialQQHandler setQQWithAppId:@"100424468" appKey:@"c7394704798a158208a74ab60104f0ba" url:url];
    //打开新浪微博的SSO开关
    [UMSocialSinaHandler openSSOWithRedirectURL:@"http://sns.whalecloud.com/sina2/callback"];
    
    //打开腾讯微博SSO开关，设置回调地址
    [UMSocialTencentWeiboHandler openSSOWithRedirectUrl:@"http://sns.whalecloud.com/tencent2/callback"];
}
- (void)messageButtonAction:(UIButton*) sender{
 
}
- (void)centreButtonAction:(UIButton*) sender{
    
}
- (void)moreButtonAction:(UIButton*) sender{
    
}
- (void)ringButtonAction:(UIButton*) sender{
    
}
- (void)settingButtonAction:(UIButton*) sender{
    
}
- (void)shareButtonAction:(UIButton*) sender{
    
}
- (void)searchButtonAction:(UIButton*) sender{
    
}
- (void)finishButtonAction:(UIButton*) sender{
    
}
- (void)showAlertView:(NSString*)title subtitle:(NSString*)subtitle
{
    __block URBAlertView *alertView = [URBAlertView dialogWithTitle:title subtitle:subtitle];
	alertView.blurBackground = YES;
//	[alertView addButtonWithTitle:@"Close"];
	[alertView addButtonWithTitle:@"确定"];
	[alertView setHandlerBlock:^(NSInteger buttonIndex, URBAlertView *alertView) {
		NSLog(@"button tapped: index=%i", buttonIndex);
		[alertView hideWithCompletionBlock:^{
			// stub
		}];
	}];
	[alertView showWithAnimation:URBAlertAnimationFlipHorizontal];

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)ringdownButtonAction:(UIButton*)button
{
    
}


@end
