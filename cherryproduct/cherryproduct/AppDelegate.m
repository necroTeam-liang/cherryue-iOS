//
//  AppDelegate.m
//  cherryproduct
//
//  Created by umessage on 14-8-4.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import "AppDelegate.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "UMSocial.h"
#import "WeiboSDK.h"
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    _window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    _window.backgroundColor = [UIColor whiteColor];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    //是否显示statusbar  ActivityIndicator（AFNetworking）
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    //显示主页
    _homeViewController = [[CPHomeViewController alloc] init];
    _navigationCongtroller = [[UINavigationController alloc] initWithRootViewController:_homeViewController];
    
    self.window.rootViewController = _navigationCongtroller;
    
    [UMSocialData setAppKey:UmengAppkey];
    
    [WeiboSDK enableDebugMode:YES];
    [WeiboSDK registerApp:kAppKey];
    
    [_window makeKeyAndVisible];
    
    //loadingView
    UIView* loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DEVICE_BOUNDS_WIDTH, DEVICE_BOUNDS_HEIGHT)];
    loadingView.backgroundColor = [UIColor colorWithRed:251.0/255.0 green:251.0/255.0 blue:248.0/255.0 alpha:0.8];
    
    
    loadingView.hidden = YES;
    loadingView.tag = 1212;
    UIImageView * view = [[UIImageView alloc] initWithFrame:CGRectMake(DEVICE_BOUNDS_WIDTH/2-133/2, DEVICE_BOUNDS_HEIGHT/2-60/2, 133, 58)];
    view.image = [UIImage imageNamed:@"loading"];
//    view.backgroundColor = [UIColor blackColor];
//    view.alpha = 0.8f;
//    view.layer.cornerRadius = 10;
    [loadingView addSubview:view];
    
    
    UIActivityIndicatorView* activity = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];//指定进度轮的大小
    [activity setCenter:CGPointMake(DEVICE_BOUNDS_WIDTH/2, DEVICE_BOUNDS_HEIGHT/2-60/2+13)];//指定进度轮中心点
    activity.tag = 1100;
    [activity setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];//设置进度轮显示类型
    [loadingView addSubview:activity];
    
//    UIButton* cancelButton = [[UIButton alloc] initWithCommonParametersAndTarget:CGRectMake(DEVICE_BOUNDS_WIDTH/2-160/2+147,DEVICE_BOUNDS_HEIGHT/2-60/2 -10-64, [UIImage imageNamed:@"layer_hud_close.png"].size.width, [UIImage imageNamed:@"layer_hud_close.png"].size.height) backGroundImage:[UIImage imageNamed:@"layer_hud_close.png"] target:_sharedClient action:@selector(cancelButtonAction:)];
//    [loadingView addSubview:cancelButton];
    
    [[UIApplication sharedApplication].keyWindow addSubview:loadingView];

    return YES;
}
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return  [UMSocialSnsService handleOpenURL:url];
}
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return  [UMSocialSnsService handleOpenURL:url];
}

- (void)didReceiveWeiboRequest:(WBBaseRequest *)request
{
    if ([request isKindOfClass:WBProvideMessageForWeiboRequest.class])
    {
//        ProvideMessageForWeiboViewController *controller = [[[ProvideMessageForWeiboViewController alloc] init] autorelease];
//        [self.viewController presentModalViewController:controller animated:YES];
    }
}

- (void)didReceiveWeiboResponse:(WBBaseResponse *)response
{
    if ([response isKindOfClass:WBSendMessageToWeiboResponse.class])
    {
        NSString *title = @"发送结果";
        NSString *message = [NSString stringWithFormat:@"响应状态: %d\n响应UserInfo数据: %@\n原请求UserInfo数据: %@",(int)response.statusCode, response.userInfo, response.requestUserInfo];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"确定"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else if ([response isKindOfClass:WBAuthorizeResponse.class])
    {
        NSString *title = @"认证结果";
        NSString *message = [NSString stringWithFormat:@"响应状态: %d\nresponse.userId: %@\nresponse.accessToken: %@\n响应UserInfo数据: %@\n原请求UserInfo数据: %@",(int)response.statusCode,[(WBAuthorizeResponse *)response userID], [(WBAuthorizeResponse *)response accessToken], response.userInfo, response.requestUserInfo];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"确定"
                                              otherButtonTitles:nil];
        
        self.wbtoken = [(WBAuthorizeResponse *)response accessToken];
        
        [alert show];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
