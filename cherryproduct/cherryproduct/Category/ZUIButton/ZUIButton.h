//
//  ZUIButton.h
//  test
//
//  Created by Enter zhao on 14-7-31.
//  Copyright (c) 2014年 ENTER. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^CommonErrorBlock)(NSError* error);
@interface ZUIButton : UIView
{
    void(^actionBlock)(id);
    UILabel* titleLabel;
    int _magnifySize;
    BOOL _isAnimate;
    CGPoint centerPoint;
    CGAffineTransform initialTransform;
}
- (instancetype)initWithFrame:(CGRect)frame animate:(BOOL)isAnimate;
-(void)setTitle:(NSString*)title textColor:(UIColor*)color textSize:(int)size;
-(void)addButtonCallBackBlock:(void(^)(id))block;
@end

