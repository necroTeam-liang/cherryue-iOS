//
//  ZUIButton.m
//  test
//
//  Created by Enter zhao on 14-7-31.
//  Copyright (c) 2014年 ENTER. All rights reserved.
//

#import "ZUIButton.h"
#import <UIKit/UIKit.h>
@implementation ZUIButton
- (instancetype)initWithFrame:(CGRect)frame animate:(BOOL)isAnimate
{
    self = [super initWithFrame:frame];
    if (self) {
        _magnifySize = 20;
        centerPoint = self.center;
        _isAnimate = isAnimate;
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _magnifySize = 20;
        centerPoint = self.center;
        _isAnimate = YES;
    }
    return self;
}
-(void)drawRect:(CGRect)rect
{
    
}
-(void)setTitle:(NSString*)title textColor:(UIColor*)color textSize:(int)size
{
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    titleLabel.text = title;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = color;
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont systemFontOfSize:size];
    [self addSubview:titleLabel];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (_isAnimate) {
        initialTransform = self.transform;
        [UIView animateWithDuration:0.2f delay:0 options:UIViewAnimationOptionLayoutSubviews animations:^{
            CGAffineTransform transform=self.transform;
            transform=CGAffineTransformScale(self.transform, 0.9,0.9);
            self.transform=transform;
            
        } completion:^(BOOL finished) {
            
        }];
    }

}
-(void)addButtonCallBackBlock:(void(^)(id))block
{
    actionBlock = block;
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (_isAnimate) {
        [UIView animateWithDuration:0.2f delay:0 options:UIViewAnimationOptionLayoutSubviews animations:^{
            self.transform=initialTransform;
            
        } completion:^(BOOL finished) {
            
        }];
    }

    
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch  locationInView:self];
    NSLog(@"x = %f,y = %f",point.x,point.y);
    
    if (point.x<=self.frame.size.width&&point.y<=self.frame.size.height)
    {
        if (actionBlock != nil)
        {
            actionBlock(self);
        }
    }
}
@end
