//
//  UIView+Animation.m
//  test
//
//  Created by Enter zhao on 14-8-1.
//  Copyright (c) 2014年 ENTER. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIView+Animation.h"
#include "math.h"
@implementation UIView (Animation)

+(void)addAnimationArrayAndPlay:(NSArray*) viewArray delay:(float)delay playType:(PLAY_TYPE)type
{
    if (!viewArray) goto ret;

    switch (type) {
        case PLAY_LIFT_RIGHT:
        {
            for (UIView* view in viewArray) {
                view.frame = CGRectMake(-view.frame.size.width, view.frame.origin.y, view.frame.size.width, view.frame.size.height);
            }
            float i = 0;
            for (UIView* view in viewArray) {
                i+=delay;
                [UIView animateWithDuration:0.3f delay:i options:UIViewAnimationOptionLayoutSubviews animations:^{
                    view.frame = CGRectMake(fabsf(view.frame.origin.x)+10, view.frame.origin.y, view.frame.size.width, view.frame.size.height);
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.2f delay:0 options:UIViewAnimationOptionLayoutSubviews animations:^{
                        view.frame = CGRectMake(fabsf(view.frame.origin.x)-10, view.frame.origin.y, view.frame.size.width, view.frame.size.height);
                    } completion:^(BOOL finished) {
                        
                    }];
                }];
            }
        }
            break;
            
        case PLAY_RIGHT_LIFT:
        {
            for (UIView* view in viewArray) {
                view.frame = CGRectMake(DEVICE_BOUNDS_WIDTH, view.frame.origin.y, view.frame.size.width, view.frame.size.height);
            }
            float i = 0;
            for (UIView* view in viewArray) {
                i+=delay;
                [UIView animateWithDuration:0.3f delay:i options:UIViewAnimationOptionLayoutSubviews animations:^{
                    view.frame = CGRectMake(fabsf(view.frame.origin.x)-10, view.frame.origin.y, view.frame.size.width, view.frame.size.height);
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.2f delay:0 options:UIViewAnimationOptionLayoutSubviews animations:^{
                        view.frame = CGRectMake(fabsf(view.frame.origin.x)+10, view.frame.origin.y, view.frame.size.width, view.frame.size.height);
                    } completion:^(BOOL finished) {
                        
                    }];
                }];
            }
        }
            break;
            
        default:
            break;
    }


    

ret:
    return;
}
@end
