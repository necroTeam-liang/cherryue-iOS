//
//  UIView+Animation.h
//  test
//
//  Created by Enter zhao on 14-8-1.
//  Copyright (c) 2014年 ENTER. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum
{
    PLAY_LIFT_RIGHT = 80011,      //
    PLAY_RIGHT_LIFT = 80012,       //
}PLAY_TYPE;
@interface UIView(Animation)

+(void)addAnimationArrayAndPlay:(NSArray*) viewArray delay:(float)delay playType:(PLAY_TYPE)type;
@end