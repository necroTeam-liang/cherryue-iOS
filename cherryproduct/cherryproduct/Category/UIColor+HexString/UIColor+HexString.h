//
//  UIColor+HexString.h
//  cherryproduct
//
//  Created by Enter zhao on 14-8-12.
//  Copyright (c) 2014年 cherry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexString)
+ (UIColor *) hexStringToColor: (NSString *) stringToConvert;
@end
