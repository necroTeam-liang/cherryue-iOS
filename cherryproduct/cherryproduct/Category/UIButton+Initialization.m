//
//  UIButton+Initialization.m
//  MTravel
//
//  Created by zhao on 14-1-23.
//  Copyright (c) 2014年 umtravelteam. All rights reserved.
//

#import "UIButton+Initialization.h"

@implementation UIButton (Initialization)

+ (UIButton*)initWithCommonParameters:(CGRect)frame backGroundImage:(UIImage*) image
{
    UIButton *commonButton = [[UIButton alloc] initWithFrame:frame];
    commonButton.backgroundColor = [UIColor clearColor];
    if (image) [commonButton setBackgroundImage:image forState:UIControlStateNormal];
    return commonButton;
}
- (UIButton*)initWithCommonParametersAndTarget:(CGRect)frame backGroundImage:(UIImage*) image target:(id)target action:(SEL)action
{
    UIButton *commonButton = [[UIButton alloc] initWithFrame:frame];
    commonButton.backgroundColor = [UIColor clearColor];
    if (image) [commonButton setBackgroundImage:image forState:UIControlStateNormal];
    if (target||action) {
        [commonButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    }
    
    return commonButton;
}


@end
