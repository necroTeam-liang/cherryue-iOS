//
//  UIButton+Initialization.h
//  MTravel
//
//  Created by zhao on 14-1-23.
//  Copyright (c) 2014年 umtravelteam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Initialization)
/*************************************************
 Function:  initWithCommonParameters
 Description:   静态初始化方法（需要释放）
 backGroundImage:背景图片
 Return: UIButton
 Others: 背景默认透明
 *************************************************/
+ (UIButton*)initWithCommonParameters:(CGRect)frame backGroundImage:(UIImage*) image;
/*************************************************
 Function:  initWithCommonParametersAndTarget
 Description:   静态初始化方法（需要释放）
 backGroundImage:背景图片
 target:响应的类
 action:回调函数
 Return: UIButton
 Others: 背景默认透明
 *************************************************/
- (UIButton*)initWithCommonParametersAndTarget:(CGRect)frame backGroundImage:(UIImage*) image target:(id)target action:(SEL)action;



@end
