//
//  UILabel+Initialization.m
//  MTravel
//
//  Created by zhao on 14-1-23.
//  Copyright (c) 2014年 umtravelteam. All rights reserved.
//

#import "UILabel+Initialization.h"
#import "Header.h"
@implementation UILabel (MTravel)

+ (UILabel*)initWithCommonParameters:(CGRect)frame text:(NSString*)text textColor:(UIColor*)textColor fontSize:(int)fontSize isNewLine:(BOOL)isNewLine{
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.text = text;
    label.frame = frame;
    label.textColor = textColor;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont systemFontOfSize:fontSize];
    label.textAlignment = NSTextAlignmentLeft;
    //判断是否换行
    if (isNewLine) {
        label.lineBreakMode = IOS7_OR_LATER?NSLineBreakByWordWrapping:UILineBreakModeWordWrap;
        label.numberOfLines = 0;
        CGSize titleSize = [text sizeWithFont:[UIFont systemFontOfSize:fontSize]
                            constrainedToSize:CGSizeMake(frame.size.width, MAXFLOAT)
                                lineBreakMode:IOS7_OR_LATER?NSLineBreakByWordWrapping:UILineBreakModeWordWrap];
        
        label.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, titleSize.height);
    }
    return label;
}

@end
