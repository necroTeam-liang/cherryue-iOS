//
//  UILabel+Initialization.h
//  MTravel
//
//  Created by zhao on 14-1-23.
//  Copyright (c) 2014年 umtravelteam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Initialization)
/*************************************************
 Function:  initWithCommonParameters
 Description:   静态初始化方法（需要释放）isNewLine = yes 可自动换行
 frame:label位置如果设置了isNewLine = yes，高度则自动改变 ，之前设置的高度将作废
 text:显示的字
 textColor:字体颜色
 fontSize:字体大小（默认system）
 isNewLine:是否换行（按照指定的宽度动态调节高度）
 Return: uilabel
 Others: 背景默认透明
 *************************************************/
+ (UILabel*)initWithCommonParameters:(CGRect)frame text:(NSString*)text textColor:(UIColor*)textColor fontSize:(int)fontSize isNewLine:(BOOL)isNewLine;


@end
